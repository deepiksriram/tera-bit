package com.project.terabit.entity;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;


// TODO: Auto-generated Javadoc
/**
 * The Class ViewedPropertyEntity.
 */
@Entity
@Table(name="Viewed_property")
@GenericGenerator(name="pkgenViewedPropertyEntity",strategy="increment")
public class ViewedPropertyEntity {
	
	/** The viewed property id. */
	@Id
	@GeneratedValue(generator="pkgenViewedPropertyEntity")
	@NotNull
	@Column(name="viewed_property_id")
	private BigInteger viewedPropertyId;
	

	@OneToOne(cascade=CascadeType.ALL,orphanRemoval=true)
	@JoinColumn(name="viewed_property_property")
	private PropertyEntity viewedPropertyProperty;
	

	/** The viewed user id. */
	@Column(name="viewed_user_id")
	private UUID viewedUserId;
	
	/** The viewed time. */
	@NotNull
	@Column(name="viewed_time")
	private LocalDateTime viewedTime;
	
	/** The viewed seller id. */
	@Column(name="viewed_seller_id")
	private BigInteger viewedSellerId;

	/**
	 * Gets the viewed property id.
	 *
	 * @return the viewed property id
	 */
	public BigInteger getViewedPropertyId() {
		return viewedPropertyId;
	}

	/**
	 * Sets the viewed property id.
	 *
	 * @param viewedPropertyId the new viewed property id
	 */
	public void setViewedPropertyId(BigInteger viewedPropertyId) {
		this.viewedPropertyId = viewedPropertyId;
	}

	/**
	 * Gets the viewed property property id.
	 *
	 * @return the viewed property property id
	 */
	public PropertyEntity getViewedPropertyPropertyId() {
		return viewedPropertyProperty;
	}

	/**
	 * Sets the viewed property property id.
	 *
	 * @param viewedPropertyPropertyId the new viewed property property id
	 */
	public void setViewedPropertyPropertyId(PropertyEntity viewedPropertyProperty) {
		this.viewedPropertyProperty = viewedPropertyProperty;
	}

	/**
	 * Gets the viewed user id.
	 *
	 * @return the viewed user id
	 */
	public UUID getViewedUserId() {
		return viewedUserId;
	}

	/**
	 * Sets the viewed user id.
	 *
	 * @param viewedUserId the new viewed user id
	 */
	public void setViewedUserId(UUID viewedUserId) {
		this.viewedUserId = viewedUserId;
	}

	/**
	 * Gets the viewed time.
	 *
	 * @return the viewed time
	 */
	public LocalDateTime getViewedTime() {
		return viewedTime;
	}

	/**
	 * Sets the viewed time.
	 *
	 * @param viewedTime the new viewed time
	 */
	public void setViewedTime(LocalDateTime viewedTime) {
		this.viewedTime = viewedTime;
	}

	/**
	 * Gets the viewed seller id.
	 *
	 * @return the viewed seller id
	 */
	public BigInteger getViewedSellerId() {
		return viewedSellerId;
	}

	/**
	 * Sets the viewed seller id.
	 *
	 * @param viewedSellerId the new viewed seller id
	 */
	public void setViewedSellerId(BigInteger viewedSellerId) {
		this.viewedSellerId = viewedSellerId;
	}
}
