package com.project.terabit.model;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.UUID;



// TODO: Auto-generated Javadoc
/**
 * The Class Cart.
 */
public class Cart {

	/** The cart id. */
	private BigInteger cartId;

	/** The cart property id. */
	private Property cartProperty;
	

	/** The cart user id. */
	private UUID cartUserId;

	/** The cart created time. */
	private LocalDateTime cartCreatedTime;

	/** The cart is active. */
	private boolean cartIsActive;

	/**
	 * Gets the cart id.
	 *
	 * @return the cart id
	 */
	public BigInteger getCartId() {
		return cartId;
	}

	/**
	 * Sets the cart id.
	 *
	 * @param cartId the new cart id
	 */
	public void setCartId(BigInteger cartId) {
		this.cartId = cartId;
	}

	/**
	 * Gets the cart property id.
	 *
	 * @return the cart property id
	 */
	public Property getCartProperty() {
		return cartProperty;
	}

	/**
	 * Sets the cart property id.
	 *
	 * @param cartPropertyId the new cart property id
	 */
	public void setCartProperty(Property cartProperty) {
		this.cartProperty = cartProperty;
	}


	/**
	 * Gets the cart user id.
	 *
	 * @return the cart user id
	 */

	public UUID getCartUserId() {
		return cartUserId;
	}


	/**
	 * Sets the cart user id.
	 *
	 * @param cartUserId the new cart user id
	 */
	public void setCartUserId(UUID cartUserId) {
		this.cartUserId = cartUserId;
	}

	/**
	 * Gets the cart created time.
	 *
	 * @return the cart created time
	 */
	public LocalDateTime getCartCreatedTime() {
		return cartCreatedTime;
	}

	/**
	 * Sets the cart created time.
	 *
	 * @param cartCreatedTime the new cart created time
	 */
	public void setCartCreatedTime(LocalDateTime cartCreatedTime) {
		this.cartCreatedTime = cartCreatedTime;
	}

	/**
	 * Checks if is cart is active.
	 *
	 * @return true, if is cart is active
	 */
	public boolean isCartIsActive() {
		return cartIsActive;
	}

	/**
	 * Sets the cart is active.
	 *
	 * @param cartIsActive the new cart is active
	 */
	public void setCartIsActive(boolean cartIsActive) {
		this.cartIsActive = cartIsActive;
	}


}
