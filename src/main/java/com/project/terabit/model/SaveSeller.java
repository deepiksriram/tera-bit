package com.project.terabit.model;

import java.math.BigInteger;
import java.util.UUID;

public class SaveSeller {
	
	private UUID userId;
	private BigInteger sellerId; 
	private BigInteger sellerPrivateContactNumber;
	private String companyName;
	private String message;
	
	
	
	public String getMessage() {
		return message;
	}
	public UUID getUserId() {
		return userId;
	}
	public void setUserId(UUID userId) {
		this.userId = userId;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public BigInteger getSellerId() {
		return sellerId;
	}
	public void setSellerId(BigInteger sellerId) {
		this.sellerId = sellerId;
	}
	public BigInteger getSellerPrivateContactNumber() {
		return sellerPrivateContactNumber;
	}
	public void setSellerPrivateContactNumber(BigInteger sellerPrivateContactNumber) {
		this.sellerPrivateContactNumber = sellerPrivateContactNumber;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	


}
