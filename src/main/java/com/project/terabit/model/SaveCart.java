package com.project.terabit.model;

import java.math.BigInteger;
import java.util.UUID;

public class SaveCart {
	
	private UUID cartUserId;
	private BigInteger cartPropertyId;
	private Boolean cartIsActive;
	
	public Boolean getCartIsActive() {
		return cartIsActive;
	}
	public void setCartIsActive(Boolean cartIsActive) {
		this.cartIsActive = cartIsActive;
	}
	public UUID getCartUserId() {
		return cartUserId;
	}
	public void setCartUserId(UUID cartUserID) {
		this.cartUserId = cartUserID;
	}
	public BigInteger getCartPropertyId() {
		return cartPropertyId;
	}
	public void setCartPropertyId(BigInteger cartPropertyId) {
		this.cartPropertyId = cartPropertyId;
	}
	
	

}
