package com.project.terabit.model;

import java.math.BigInteger;
import java.util.UUID;

public class ShareProperty {

	
	private UUID userId;
	private String receiverEmailId;
	private String description;
	private BigInteger propertyId;
	public BigInteger getPropertyId() {
		return propertyId;
	}
	public void setPropertyId(BigInteger propertyId) {
		this.propertyId = propertyId;
	}
	public UUID getUserId() {
		return userId;
	}
	public void setUserId(UUID userId) {
		this.userId = userId;
	}
	public String getReceiverEmailId() {
		return receiverEmailId;
	}
	public void setReceiverEmailId(String receiverEmailId) {
		this.receiverEmailId = receiverEmailId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
