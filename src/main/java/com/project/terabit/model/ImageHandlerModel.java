package com.project.terabit.model;

import java.math.BigInteger;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class ImageHandlerModel {
	

	private String userId;
	private BigInteger propertyId;
	private BigInteger sellerId;
	private String imageDescription;
	private String imagename;
	private BigInteger imageId;
	private MultipartFile imagetobesaved;
	private List<MultipartFile> propertyimages;
	

	public List<MultipartFile> getPropertyimages() {
		return propertyimages;
	}

	public void setPropertyimages(List<MultipartFile> propertyimages) {
		this.propertyimages = propertyimages;
	}

	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public MultipartFile getImagetobesaved() {
		return imagetobesaved;
	}

	public void setImagetobesaved(MultipartFile imagetobesaved) {
		this.imagetobesaved = imagetobesaved;
	}

	
	public BigInteger getImageId() {
		return imageId;
	}



	public void setImageId(BigInteger imageId) {
		this.imageId = imageId;
	}
	
	
	public BigInteger getSellerId() {
		return sellerId;
	}



	public void setSellerId(BigInteger sellerId) {
		this.sellerId = sellerId;
	}



	public String getImageDescription() {
		return imageDescription;
	}



	public void setImageDescription(String imageDescription) {
		this.imageDescription = imageDescription;
	}



	public BigInteger getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(BigInteger propertyId) {
		this.propertyId = propertyId;
	}

	public String getImagename() {
		return imagename;
	}

	public void setImagename(String imagename) {
		this.imagename = imagename;
	}
	

}
