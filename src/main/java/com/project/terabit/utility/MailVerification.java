package com.project.terabit.utility;


import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.MimeMessageHelper;

import com.project.terabit.model.MailModel;  

public class MailVerification {
	
	private static final String MAILFROM="mailFrom";
	private static final String PASSKEY="password";
	private static final String LOGINLINK="MAILLINK.login_link";
	private static final String IMG="https://www.thebalance.com/thmb/hrlgAOeCK8hAnrSLb9af7g9qanA=/950x0/real-estate-what-it-is-and-how-it-works-3305882-FINAL-19013d1ad78f4d2d90ced41faa077a85.jpg";
	
	/** The property. */
	//****Initializing:		Property # # #
	Properties prop =new Properties();						
	
	/** The input stream. */
	//*****Initializing and populating inputstream with application.properties
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");	

	
//	private MailVerification() {
//	    throw new IllegalStateException("Utility class");
//	  }
	public  void sentmail(MailModel mailtobesent) throws Exception
	{
		
		try {
			prop.load(inputStream);
			if(mailtobesent.getLink()==null)
				mailtobesent.setLink(prop.getProperty(LOGINLINK));
			
			Properties props = new Properties();   
			props.put("mail.smtp.host", "smtp.gmail.com");  
			props.put("mail.smtp.socketFactory.port", "465");  
			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
			props.put("mail.smtp.auth", "true");  
			props.put("mail.smtp.port", "465"); 
			props.put("mail.smtp.ssl.checkserveridentity", true);

			   Session session = Session.getInstance(props,    
			           new javax.mail.Authenticator() {   
				   	
				   		@Override
			           protected PasswordAuthentication getPasswordAuthentication() {  
			           return new PasswordAuthentication(prop.getProperty(MAILFROM),prop.getProperty(PASSKEY));  
			           }    
			          });  
			  
			
			MimeMessage message = new MimeMessage(session);    
	        message.addRecipient(Message.RecipientType.TO,new InternetAddress(mailtobesent.getMailto()));    
	        message.setSubject(mailtobesent.getSub()); 
	        MimeMessageHelper mimemessagehelper=new MimeMessageHelper(message,true,"UTF-8");
	        mimemessagehelper.setText("<body >\r\n" + 
	           		"\r\n" + 
	           		"	<div style=\"background-color:lightblue\"><center><h2>Background Image</h2></center></div>\r\n" + 
	           		"\r\n" + 
	           		"	<IMG src='"+IMG+"'width=100% height=70%>\r\n" + 
	           		"<p style=\"background-color: white\">"+mailtobesent.getBody()+"</p>\r\n" + 
	           		"<a href=\""+mailtobesent.getLink()+"\"> Click here</a>" + 
	           		"\r\n" + 
	           		"\r\n" + 
	           		"</body>", true);
	        
	          
	        Transport.send(message);    
	    	
		}
		catch(Exception e)
		{
			System.out.println(e);
			
		}
		
	}
	
	
	
}
