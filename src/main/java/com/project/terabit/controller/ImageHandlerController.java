package com.project.terabit.controller;


import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Image;
import com.project.terabit.model.ImageHandlerModel;
import com.project.terabit.repository.ImageRepository;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.service.ImageHandlerService;
import com.project.terabit.service.ServiceException;  

@RestController
@RequestMapping(value="terabit/api/v1/imagefolder")
@CrossOrigin(origins = "*")
class ImageHandler
{
	
	/** The log. */
	//*****Initializing:	log with the object of the current class(AdminController)	# # #
	Logger log=LoggerFactory.getLogger(this.getClass());		
	
	/** The property. */
	//****Initializing:		Property # # #
	Properties property =new Properties();						
	
	/** The input stream. */
	//*****Initializing and populating inputstream with application.properties
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");	
	
	private static final String CREATIONSUCCESSMESSAGE = "Image successfully created";
	
	
	private static final String DELETIONSUCCESSMESSAGE = "Image successfully deleted";
	
	private static final String RESPONSE="ERROR : ";
	
	@Autowired 
	ImageRepository imagerepository;
	
	@Autowired
	PropertyRepository propertyrepository;
	
	@Autowired
	UserRepository userrepository;
	
	@Autowired 
	ImageHandlerService imagehandlerservices;
	
	@PostMapping(value= {"/save/","/save/{saltstring}"})
	public Image saveImage(@PathVariable (name="saltstring") String saltstring,@ModelAttribute ImageHandlerModel imagetobesaved ) throws Exception
    {   
        String status="Failed";
        
       
       Image imagetobesave=new Image();
         
        try
        {
            
        	imagetobesave=imagehandlerservices.saveImage(imagetobesaved, saltstring);
        	imagetobesave.setMessage(CREATIONSUCCESSMESSAGE);

        } 
        
        catch(ServiceException e) {
        	
        	property.load(inputStream); 
			status=RESPONSE+property.getProperty(e.getMessage());
			imagetobesave.setMessage(status);
			log.error(status);
        	
        }
       
        catch (Exception e) 
        {
        	property.load(inputStream); 
			status=RESPONSE+property.getProperty(e.getMessage());
			imagetobesave.setMessage(status);
			log.error(status);
        }
         
       return imagetobesave;
    }
	@DeleteMapping(value= {"/delete/","/delete/{saltstring}"})
	public Image deleteImage(@PathVariable (name="saltstring") String saltstring,@ModelAttribute ImageHandlerModel imagetobesaved ) throws Exception
    {   
        String status="Failed";
        
       
        Image imagetobedeleted=new Image();
         
        try
        {
            
        	imagetobedeleted=imagehandlerservices.deleteImage(imagetobesaved, saltstring);
        	imagetobedeleted.setMessage(DELETIONSUCCESSMESSAGE);

        } 
        catch(ServiceException e) {
        	
        	property.load(inputStream); 
			status=RESPONSE+property.getProperty(e.getMessage());
			imagetobedeleted.setMessage(status);
			log.error(status);
        	
        }
        catch (Exception exception) 
        {
        	property.load(inputStream); 
        	status =RESPONSE+property.getProperty(exception.getMessage());
        	imagetobedeleted.setMessage(status);
			log.error(status);
        }
         
       return imagetobedeleted;
    }

	@PostMapping(value= {"/saves/","/saves/{saltstring}"})
	public List<Image> saveImages(@PathVariable (name="saltstring") String saltstring,@ModelAttribute ImageHandlerModel imagetobesaved ) throws Exception
    {   
        String status="Failed";
        
       
       List<Image> imagetobesave=new ArrayList<>();
         
        try
        {
        	
        	imagetobesave=imagehandlerservices.savePropertyImages(imagetobesaved, saltstring);
        	//imagetobesave.setMessage(CREATIONSUCCESSMESSAGEFORMULTIPLEIMAGE);

        } 
        
        catch(ServiceException e) {
        	
        	property.load(inputStream); 
			status=RESPONSE+property.getProperty(e.getMessage());
			
			log.error(status);
        	
        }
       
        catch (Exception e) 
        {
        	property.load(inputStream); 
			status=RESPONSE+property.getProperty(e.getMessage());
			
			log.error(status);
        }
         
       return imagetobesave;
    }
}
  

		
	

 