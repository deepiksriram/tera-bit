package com.project.terabit.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.terabit.model.Property;
import com.project.terabit.model.ShareProperty;
import com.project.terabit.service.ServiceException;
import com.project.terabit.service.SharePropertyServiceImpl;

@RestController
@RequestMapping(value="/terabit/api/v1/share")
@CrossOrigin("*")
public class SharePropertyController {

	/** The log. */
	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired(required=true)
	private SharePropertyServiceImpl sharePropertyService;
	
	/** The prop. */
	Properties prop = new Properties(); 
	
	/** The input stream. */
	InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
	
	private static final String SHAREPROPERTYSUCCESS="The property has been shared successfully";
	
	@PostMapping(value={"/shareProperty/","/shareProperty/{saltstring}"})
	public Property shareProperty(@PathVariable(name="saltstring" ,required=false) String saltstring,@RequestBody ShareProperty shareProperty) throws ServiceException,IOException {
		Property returnProperty = new Property();
		String response=null;
		try {
			log.info("incoming details....");
			log.info("user id: "+shareProperty.getUserId()+" ,"+"property id: "+shareProperty.getPropertyId()+" ,"+"mail id: "+shareProperty.getReceiverEmailId());
			returnProperty = sharePropertyService.shareProperty(shareProperty,saltstring);
			response="Success: ";
			returnProperty.setMessage(response+SHAREPROPERTYSUCCESS);
			log.info(returnProperty.getMessage());
		}catch(Exception exception) {
			
			response="Error:";
			prop.load(inputStream); 
			returnProperty.setMessage(response+prop.getProperty(exception.getMessage()));
			String error=response+" "+prop.getProperty(exception.getMessage());
			returnProperty.setMessage(error);
			log.error(error);
			
		}
		return returnProperty;
	}
	
}
