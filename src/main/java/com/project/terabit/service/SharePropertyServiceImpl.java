package com.project.terabit.service;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.model.MailModel;
import com.project.terabit.model.Property;
import com.project.terabit.model.ShareProperty;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.utility.MailVerification;
import com.project.terabit.validator.SharePropertyValidator;


@Service
public class SharePropertyServiceImpl implements SharePropertyService{

	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PropertyRepository propertyRepository;
	
	private static final String SERVICEEXCEPTION_NOUSERID = "UTILITYSERVICE.no_userid_provided";
	
	private static final String SERVICEEXCEPTION_NOPROPERTYID = "SHAREPROPERTYSERVICE.no_propertyid_provided";
	
	private static final String SERVICEEXCEPTION_NOEMAILID = "SHAREPROPERTYSERVICE.no_emailid_provided";
	
	private static final String SERVICEEXCEPTION_INVALIDUSERID = "SHAREPROPERTYSERVICE.userid_not_found";
	
	private static final String SERVICEEXCEPTION_INVALIDPROPERTYID = "SHAREPROPERTYSERVICE.propertyid_not_found";
	
	private static final String SERVICEEXCEPTION_SALTSTRINGNULL = "SHAREPROPERTYSERVICE.saltstring_null";
	
	private static final String MAILBODYFORSHARE = " has shared you a property posted in Tera-bit. Please click on the below link to view the property.";
	
	private static final String MAILSUBJECTFORSHARE = "A property has been shared to you";
	
	private static final String SHAREPROPERTYLINK="http://localhost:4200/home";
				
	@Override
	public Property shareProperty(ShareProperty shareProperty, String saltstring) throws Exception{
	
		Property returnProperty = new Property();
		try {
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_SALTSTRINGNULL);
			}
			log.info("saltstring: "+saltstring);
			if(shareProperty.getUserId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			log.info("user id passed: "+shareProperty.getUserId());
			if(shareProperty.getPropertyId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOPROPERTYID);
			}
			log.info("property id passed: "+shareProperty.getPropertyId());
			if(shareProperty.getReceiverEmailId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOEMAILID);
			}
			log.info("email id passed: "+shareProperty.getReceiverEmailId());
			SharePropertyValidator.validate(shareProperty);
			String senderName = userRepository.findUserNameFromUserId(shareProperty.getUserId(),saltstring);
			if(senderName==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDUSERID);
			}
			log.info("name of the sender passed: "+senderName);
			PropertyEntity propertyEntity = propertyRepository.getPropertyByPropertyId(shareProperty.getPropertyId());
			if(propertyEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDPROPERTYID);
			}
			
			MailModel mailmodel=new MailModel();
			mailmodel.setLink(SHAREPROPERTYLINK);
			mailmodel.setMailto(shareProperty.getReceiverEmailId());
			mailmodel.setBody("Hi,"+"<br>"+senderName+MAILBODYFORSHARE);
			mailmodel.setSub(MAILSUBJECTFORSHARE);
			MailVerification mailVerification =new MailVerification();
			mailVerification.sentmail(mailmodel);
			
			PropertyServiceImpl propertyService = new PropertyServiceImpl();
			returnProperty = propertyService.propertyForViewedProperty(propertyEntity);
			
		}
		catch(ServiceException exception){
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("ShareProperty"+exception.getMessage());
			throw exception;
		}
		return returnProperty;
	}
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}
}
