package com.project.terabit.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.project.terabit.model.Image;
import com.project.terabit.model.ImageHandlerModel;

@Service
public interface ImageHandlerService {

	public Image saveImage(ImageHandlerModel imagetobesaved,String saltstring) throws ServiceException,Exception;
	public List<Image> savePropertyImages(ImageHandlerModel imagetobesaved,String saltstring) throws ServiceException,Exception;
	public Image deleteImage(ImageHandlerModel imagetobesaved,String saltstring) throws ServiceException,Exception;
	
}
