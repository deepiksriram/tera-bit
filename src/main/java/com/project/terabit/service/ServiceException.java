package com.project.terabit.service;



/**
 * The Class ServiceException.
 */
public class ServiceException extends Exception{
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new service exception as string.
	 *
	 * @param message the exception message ass string
	 */
	public ServiceException(String message) {
		super(message);
	}
}



