package com.project.terabit.service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.AdminEntity;
import com.project.terabit.entity.CartEntity;
import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.AddPropertyUser;
import com.project.terabit.model.CreateSeller;
import com.project.terabit.model.ImageHandlerModel;
import com.project.terabit.model.MailModel;
import com.project.terabit.model.SaveSeller;
import com.project.terabit.model.Seller;
import com.project.terabit.model.User;
import com.project.terabit.repository.AdminRepository;
import com.project.terabit.repository.CartRepository;
import com.project.terabit.repository.NotificationRepository;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.utility.MailVerification;
import com.project.terabit.validator.SellerValidator;



/**@author deepik sriram & solai ganesh
/**
 * The Class SellerServiceImpl.
 * CRUD operations for SELLER is implemented in this class
 */
@Service
@Transactional(readOnly=true)

public class SellerServiceImpl implements SellerService{
	

	
	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());

	/** Autowiring the user repository */
	//***** Autowiring the UserRepository # # #
	@Autowired
	UserRepository userRepository;
	
	/** Autowiring the seller repository. */
	@Autowired
	private SellerRepository sellerRepository;
	
	@Autowired
	private AdminRepository adminRepository;
	
	@Autowired
	NotificationRepository notificationRepository;

	
	@Autowired
	PropertyRepository propertyRepository;

	@Autowired
	CartRepository cartRepository;
	
	//****UTILITY SERVICE EXCEPTION
	
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "SELLERSERVICE.no_saltstring";	

	
	/** The Constant SERVICEEXCEPTION_NotA_Seller. */
	//***** Message related to Service Exception
	private static final String SERVICEEXCEPTION_INVALIDSELLER = "UPDATESELLERSERVICE.ServiceException_NotA_Seller";
	
	/** The Constant SERVICEEXCEPTION_UserIsNotActiveSeller. */
	private static final String SERVICEEXCEPTION_USERNOTSELLER="UPDATESELLERSERVICE.ServiceException_UserIsNotSeller";
	


	
	/** The Constant SERVICEEXCEPTION_NOUSER. */
	private static final String SERVICEEXCEPTION_NOUSER = "CREATESELLERSERVICE.No_User_Exists";
	
	/** The Constant SERVICEEXCEPTION_EXISTINGSELLER. */
	private static final String SERVICEEXCEPTION_EXISTINGSELLER = "CREATESELLERSERVICE.Seller_already_exists";
	
	/** The Constant SERVICEEXCEPTION_EXISTINGCONTACTNO. */
	private static final String SERVICEEXCEPTION_EXISTINGCONTACTNO = "CREATESELLERSERVICE.ContactNo_already_exists";
	

	/** The Constant SERVICEEXCEPTION_URLMISSMATCH. */
	private static final String SERVICEEXCEPTION_URLMISSMATCH = "CREATESELLERSERVICE.invalid_saltstring";
	
	
	/** The Constant SERVICEEXCEPTION_NOUSERID. */
	private static final String SERVICEEXCEPTION_NOUSERID = "CREATESELLERSERVICE.invalid_user_id";
	
	
	/** The Constant SERVICEEXCEPTION_SELLERISNOTACTIVE. */
	private static final String SERVICEEXCEPTION_SELLERISNOTACTIVE = "DELETESELLERSERVICE.Not_Active_seller";
	
	/** The Constant SERVICEEXCEPTION_USER_IS_NOT_A_SELLER. */
	private static final String SERVICEEXCEPTION_USER_IS_NOT_A_SELLER = "DELETESELLERSERVICE.User_is_not_seller";
	
	private static final String SERVICEEXCEPTION_NOSELLERFORPROPERTY = "GETSELLERBYPROPERTY.no_seller";

	private static final String SERVICEEXCEPTION_NOADMINID = "CREATESELLERSERVICE.no_admin_id";
	
	private static final String SERVICEEXCEPTION_NOADMIN = "SELLERSERVICE.no_admin";
	
	private static final String SERVICEEXCEPTION_INVALIDADMIN = "SELLERSERVICE.invalid_adminid";

	
	private static final String SERVICEEXCEPTION_CONTACTNUMBERALREADYEXISTS = "UPDATESERVICE.invalid_contactNumber";
	
	private static final String SERVICEEXCEPTION_NOUSERFORSELLER = "GETSELLERBYPROPERTY.seller_not_seller";

	private static final String SERVICEEXCEPTION_NOPROPERTY = "SELLERSERVICE.no_property_for_propertyid";
	private static final String SERVICEEXCEPTION_NOPROPERTYID = "SELLERSERVICE.no_propertyid";

	
	private static final String SELLERCREATIONNOTIFICATIONTOADMIN = " has requested to be a seller. Please approve or reject it";
	
	private static final String CONTACTSELLERNOTIFICATION = " has tried to contact your seller for the property located near ";
	
	private static final String NOTIFICAITONFORSELLERCREATIONTOSELLER = "You have requested to be a seller";
	
	private static final String DELETESELLERNOTIFICATIONTOADMIN = " has revoked their seller access";
	
	private static final String DELETESELLERNOTIFICATIONTOSELLER = "You have revoked their seller access";
	
	private static final String CONTACTSELLERNOTIFICATIONTOSELLER = "Some one tried to contact you for a property. Contact your admin for more details";
	
	private static final String CREATESELLERSUBJECTTOADMIN = "A user is requesting your approval to be a seller";

	private static final String DELETESELLERSUBJECTTOADMIN = "Alert: One of your seller has revoked his seller access";

	private static final String CONTACTSELLERSUBJECTTOSELLER = "Alert: Some one tried to contact you for a property";
			
	private static final String CONTACTSELLERSUBJECTTOADMIN = "Alert: Some one tried to contact your seller for a property";

	/* (non-Javadoc)
	 * @see com.project.terabit.service.SellerService#createSeller(java.lang.String, com.project.terabit.model.CreateSeller)
	 */
	/** Over riding createSeller of service method.
	 * Creating SELLER for USER
	 */
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User createSeller(String saltstring,CreateSeller createseller) throws Exception {
		//creating sellerentity object to set in the db
		SellerEntity sellerEntityToBeAdded = new SellerEntity();
		
		//declaring sellerentity to retrive value from db 
		SellerEntity sellerEntity;
		User user = new User();
		try {
			
			//checking if saltstring is null
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			
			//checking if user id is null
			if(createseller.getUserId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);

			}
			if(createseller.getSellerRightBy()==null) {
				throw new  ServiceException(SERVICEEXCEPTION_NOADMINID);
			}
			AdminEntity adminEntity = adminRepository.getAdminByAdminId(createseller.getSellerRightBy());
			if(adminEntity==null) {
				throw new  ServiceException(SERVICEEXCEPTION_NOADMIN);
			}

			//retriving user data from user entity
			UsersEntity userEntity = userRepository.findActiveUsers(createseller.getUserId());
			
			//checking is there is no user for given user id
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			
			//checking if saltstring is matching with the value in the db
			if(!(saltstring.equals(userEntity.getSaltString()))){
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			
			//checking if it is a existing seller
			if((userEntity.getUserSellerId()!=null)&&(userEntity.isUserIsSeller())){ 
	            throw new ServiceException(SERVICEEXCEPTION_EXISTINGSELLER);
			}


			//checking if the private contact number is there
			if(createseller.getSellerPrivateContact()!=null) {
				
				//validating the details
				SellerValidator.validate(createseller);
				
				//retriving data from seller repository
				sellerEntity = sellerRepository.findSellerByContactNumber(createseller.getSellerPrivateContact());
				
				//checking if there is contact number already exists
				if(sellerEntity!=null) {
					throw new ServiceException(SERVICEEXCEPTION_EXISTINGCONTACTNO);
				}
			}
			//populating value in the sellerentity
			sellerEntityToBeAdded.setSellerCreatedTime(LocalDateTime.now());
			sellerEntityToBeAdded.setSellerIsActive(false);
			sellerEntityToBeAdded.setSellerModifiedTime(LocalDateTime.now());
			sellerEntityToBeAdded.setSellerRightBy(createseller.getSellerRightBy());
			sellerEntityToBeAdded.setSellerPropertyCount(BigInteger.valueOf(0));
			
			//checking if the private contact number is there
			//setting the contact number from front end if passed
			if(createseller.getSellerPrivateContact()==null) {
				sellerEntityToBeAdded.setSellerPrivateContact(userEntity.getUserContactNo());
			}
			
			//setting the contact number from user if not passed from front end
			else if(createseller.getSellerPrivateContact()!=null) {
				SellerValidator.validatePrivateContactNumber(createseller.getSellerPrivateContact());
				sellerEntityToBeAdded.setSellerPrivateContact(createseller.getSellerPrivateContact());
			}
			if(createseller.getSellerCompanyName()==null) {
				sellerEntityToBeAdded.setSellerCompanyName(userEntity.getUserFirstName()+" "+userEntity.getUserLastName());
			}
			else {
				sellerEntityToBeAdded.setSellerCompanyName(createseller.getSellerCompanyName());
			}
			//populating sellerentity in user entity
			userEntity.setUserSellerId(sellerEntityToBeAdded);	
			userEntity.setUserIsSeller(true);
			sellerRepository.save(sellerEntityToBeAdded);
			userRepository.save(userEntity);
			
			String userid = adminRepository.getUserByAdminId(createseller.getSellerRightBy());
			if(userid==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDADMIN);
			}

			UsersEntity userEntityToNotify = userRepository.findUserByUserId(UUID.fromString(userid));
			NotificationEntity notificationEntity = new NotificationEntity();
			notificationEntity.setNotificationTo(userEntityToNotify.getUserId());
			notificationEntity.setNotificationContent(sellerEntityToBeAdded.getSellerCompanyName()+SELLERCREATIONNOTIFICATIONTOADMIN);
			notificationEntity.setNotificationIsActive(true);
			notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
			notificationEntity.setNotificationViewedTime(LocalDateTime.now());
			notificationRepository.save(notificationEntity);
			
			userEntityToNotify.getUserNotificationIds().add(notificationEntity);
			userRepository.save(userEntityToNotify);
			
			NotificationEntity notificationEntityForSeller = new NotificationEntity();
			notificationEntityForSeller.setNotificationTo(createseller.getUserId());
			notificationEntityForSeller.setNotificationContent(NOTIFICAITONFORSELLERCREATIONTOSELLER);
			notificationEntityForSeller.setNotificationIsActive(true);
			notificationEntityForSeller.setNotificationNotifiedTime(LocalDateTime.now());
			notificationEntityForSeller.setNotificationViewedTime(LocalDateTime.now());
			notificationRepository.save(notificationEntityForSeller);
			
			MailModel mailmodel=new MailModel();
			mailmodel.setMailto(userEntityToNotify.getUserEmailId());
			mailmodel.setBody(notificationEntity.getNotificationContent());
			mailmodel.setSub(CREATESELLERSUBJECTTOADMIN);
			MailVerification mailVerification =new MailVerification();
			mailVerification.sentmail(mailmodel);
			
			
			
			userEntity.getUserNotificationIds().add(notificationEntityForSeller);
			userRepository.save(userEntity);
			//creating the seller to be returned
			Seller seller = new Seller();
			
			//populating values from db
			seller.setSellerCreatedTime(sellerEntityToBeAdded.getSellerCreatedTime());
			seller.setSellerId(sellerEntityToBeAdded.getSellerId());
			seller.setSellerPrivateContact(sellerEntityToBeAdded.getSellerPrivateContact());
			seller.setSellerPropertyCount(sellerEntityToBeAdded.getSellerPropertyCount());
			seller.setSellerRightBy(sellerEntityToBeAdded.getSellerRightBy());
			
			LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
			
			user.setUserId(userEntity.getUserId());
			user.setUserSellerId(loginservice.sellerForUser(userEntity));
			user.setUserAuthenticated(userEntity.isIsuserAuthenticated());
			user.setUserCarts(loginservice.cartForUser(userEntity.getUserCarts()));
			user.setUserContactNo(userEntity.getUserContactNo());
			user.setUserEmailId(userEntity.getUserEmailId());
			user.setUserCreatedTime(userEntity.getUserCreatedTime());
			user.setUserFeedbacks(loginservice.feedback(userEntity.getUserFeedbacks()));
			user.setUserFirstName(userEntity.getUserFirstName());
			user.setUserLastName(userEntity.getUserLastName());
			if(userEntity.getUserImage()!=null)
				user.setUserImage(loginservice.imageForUser(userEntity.getUserImage()));
			user.setUserNotificationIds(loginservice.notificationlist(userEntity.getUserNotificationIds()));
			user.setUserIsActive(userEntity.isUserIsActive());
			user.setUserIsSeller(userEntity.isUserIsSeller());
			user.setUserVerifed(userEntity.isUserVerifed());
			user.setUserViewedProperties(loginservice.viewedproperty(userEntity.getUserViewedProperties(), userEntity.getUserId()));
			
			
			//returning the user
			return user;
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
			
			//***** Throwing the ServiceException
			throw serviceException;
		}
		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
			
			//***** Throwing the Exception
			throw exception;
		}
		
	}


	//***** OverRiding the updateSeller method of SellerService
	
	/* (non-Javadoc)
	 * @see com.project.terabit.service.SellerService#updateSeller(com.project.terabit.model.SaveSeller, java.lang.String)
	 */
	/** Over riding createSeller of service method.
	 * Updating existing SELLER
	 */
	@Override
	@Transactional(readOnly=false,propagation =Propagation.REQUIRES_NEW)
	public Seller updateSeller(SaveSeller sellerToUpdated,String saltString) throws ServiceException,Exception
	{
	
		try {
			//**** Checking if the saltstring passed is empty
			if(saltString==null) 
			{
				
				throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
				
			}
			if(sellerToUpdated.getUserId()==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			
			
			Seller sellerUpdated =new Seller();
			
			//***** Getting the UserEntity from User repository JPA # # # 
			UsersEntity sellerToBeUpdatedEntity =userRepository.findActiveUsers(sellerToUpdated.getUserId());
		
			//***** Checking if the user Exist or Not if yes throe an exxception to the user # # #
			if(sellerToBeUpdatedEntity==null)
			{
				
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
		
			//***** if saltString does not match throws a exception # # #
			if(!sellerToBeUpdatedEntity.getSaltString().equals(saltString))
			{
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
		
			//***** if user is not having a sellerObject throw an exception # # #
			if(sellerToBeUpdatedEntity.getUserSellerId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDSELLER);
			}
			//**** throw exception when user is not an active seller # # #
			if(!sellerToBeUpdatedEntity.isUserIsSeller())
			{
				throw new ServiceException(SERVICEEXCEPTION_USERNOTSELLER);
			}

			
			// ***** Validating the Seller Phone Number # # # 
			
			//***** updating the Seller contact # # #
			if(sellerToUpdated.getSellerPrivateContactNumber()!=null) {
				UsersEntity userEntity = userRepository.checkUserContactNumber(sellerToUpdated.getSellerPrivateContactNumber());
				if(userEntity!=null  && !(userEntity.getUserId().toString().equals(sellerToUpdated.getUserId().toString()))) {
					throw new ServiceException(SERVICEEXCEPTION_CONTACTNUMBERALREADYEXISTS);
				}
				SellerValidator.validatePrivateContactNumber(sellerToUpdated.getSellerPrivateContactNumber());
				sellerToBeUpdatedEntity.getUserSellerId().setSellerPrivateContact(sellerToUpdated.getSellerPrivateContactNumber());
			}
			if(sellerToUpdated.getCompanyName()!=null) {
				sellerToBeUpdatedEntity.getUserSellerId().setSellerCompanyName(sellerToUpdated.getCompanyName());
			}
			
			//***** saving the changes into the dataBase # # #
			userRepository.save(sellerToBeUpdatedEntity);
			
			sellerUpdated.setSellerCompanyName(sellerToBeUpdatedEntity.getUserSellerId().getSellerCompanyName());
			sellerUpdated.setSellerCreatedTime(sellerToBeUpdatedEntity.getUserSellerId().getSellerCreatedTime());
			sellerUpdated.setSellerId(sellerToBeUpdatedEntity.getUserSellerId().getSellerId());
			sellerUpdated.setSellerPrivateContact(sellerToBeUpdatedEntity.getUserSellerId().getSellerPrivateContact());
			sellerUpdated.setSellerIsActive(sellerToBeUpdatedEntity.getUserSellerId().isSellerIsActive());
			sellerUpdated.setSellerRightBy(sellerToBeUpdatedEntity.getUserSellerId().getSellerRightBy());
			
			
			return sellerUpdated;
			
			
			
			
		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
		
		
	}
	
	/* (non-Javadoc)
	 * @see com.project.terabit.service.SellerService#deleteSeller(com.project.terabit.model.User, java.lang.String)
	 */
	/** Over riding createSeller of service method.
	 * Deleting the existing SELLER 
	 */	
	@Override
	@Transactional(readOnly=false,propagation=Propagation.REQUIRES_NEW)
		
	public User deleteSeller(User user, String saltString) throws Exception {
			User userToBeReturned = new User();
			UsersEntity userEntity = new UsersEntity();
			Seller sellerToBeReturned = new Seller();
			try {
					if(saltString==null) 
					{
						throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
					}
					if(user.getUserId()==null) {
						throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
					}
						
					//*****Obtaining UserEntiy Object from User repository # # #
					UsersEntity sellerToBeDeletedEntity=userRepository.findActiveUsers(user.getUserId());
					
					
					
					//***** Checking if the user Exist or Not if yes throw an exception to the user # # #
					if(sellerToBeDeletedEntity==null)
					{
							throw new ServiceException(SERVICEEXCEPTION_NOUSER);
					}
					
					//****** Checking if the saltstring passed matches with the db
					if(!(sellerToBeDeletedEntity.getSaltString().equals(saltString)))
					{
							throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
					}
					
					//***** Check if the user is an existing seller
					if(!sellerToBeDeletedEntity.isUserIsSeller())
					{
						throw new ServiceException(SERVICEEXCEPTION_USER_IS_NOT_A_SELLER);
					}
					
					
					//****** Checking if the seller is active
					if(!sellerToBeDeletedEntity.getUserSellerId().isSellerIsActive())
					{
						throw new ServiceException(SERVICEEXCEPTION_SELLERISNOTACTIVE);
					}
					
					sellerToBeDeletedEntity.setUserIsSeller(false);
					sellerToBeDeletedEntity.getUserSellerId().setSellerIsActive(false);
					List<PropertyEntity> propertyToBeDeleted = propertyRepository.findPropertyBySeller(sellerToBeDeletedEntity.getUserSellerId().getSellerId());
					
					if(propertyToBeDeleted!=null && !propertyToBeDeleted.isEmpty()) {
						for(PropertyEntity propertyEntity: propertyToBeDeleted) {
							ImageHandlerServiceImpl imagehandlerservice = new ImageHandlerServiceImpl();
							if(propertyEntity.getPropertyImageId()!=null && !propertyEntity.getPropertyImageId().isEmpty()) {
								ImageHandlerModel imageHandler = new ImageHandlerModel();
								imageHandler.setUserId(sellerToBeDeletedEntity.getUserId().toString());
								imageHandler.setPropertyId(propertyEntity.getPropertyId());
								imageHandler.setSellerId(sellerToBeDeletedEntity.getUserSellerId().getSellerId());
								for (ImageEntity imageEntity : propertyEntity.getPropertyImageId()) {
									imageHandler.setImageId(imageEntity.getImageId());
									imagehandlerservice.deleteImage(imageHandler, sellerToBeDeletedEntity.getSaltString());
								}
							}
							propertyEntity.setPropertyIsActive(false);
							propertyRepository.save(propertyEntity);
						}
					}
					
					if(sellerToBeDeletedEntity.getUserSellerId().getSellerNotificationId()!=null && !sellerToBeDeletedEntity.getUserSellerId().getSellerNotificationId().isEmpty()) {
						for(NotificationEntity notificationEntity:sellerToBeDeletedEntity.getUserSellerId().getSellerNotificationId()) {
							notificationEntity.setNotificationIsActive(false);
						}
					}
					
					if(sellerToBeDeletedEntity.getUserAdminId()!=null && sellerToBeDeletedEntity.getUserAdminId().isAdminIsActive()) {
						sellerToBeDeletedEntity.getUserAdminId().setAdminIsActive(false);
						log.info("user is an admin");
						log.info("admin id: "+sellerToBeDeletedEntity.getUserAdminId());
						List<SellerEntity> sellerToBeDeleted = sellerRepository.getSellerByAdmin(sellerToBeDeletedEntity.getUserAdminId().getAdminId());
						if(sellerToBeDeleted!=null && !sellerToBeDeleted.isEmpty()) {
							for(SellerEntity sellerEntity: sellerToBeDeleted) {
								sellerEntity.setSellerIsActive(false);
								UsersEntity userEntityForSeller = userRepository.findUserFromSellerId(sellerEntity.getSellerId());
								List<PropertyEntity> propertyToBeDeletedForAdmin = sellerEntity.getSellerPropertyId();
								if(propertyToBeDeletedForAdmin!=null && !propertyToBeDeletedForAdmin.isEmpty()) {
									for(PropertyEntity propertyEntity: propertyToBeDeletedForAdmin) {
										ImageHandlerServiceImpl imagehandlerservice = new ImageHandlerServiceImpl();
										if(propertyEntity.getPropertyImageId()!=null && !propertyEntity.getPropertyImageId().isEmpty()) {
											ImageHandlerModel imageHandler = new ImageHandlerModel();
											imageHandler.setUserId(userEntityForSeller.getUserId().toString());
											imageHandler.setPropertyId(propertyEntity.getPropertyId());
											imageHandler.setSellerId(sellerEntity.getSellerId());
											for (ImageEntity imageEntity : propertyEntity.getPropertyImageId()) {
												imageHandler.setImageId(imageEntity.getImageId());
												imagehandlerservice.deleteImage(imageHandler, userEntityForSeller.getSaltString());
											}
										}
										
										List<CartEntity> cartEntityList = cartRepository.findCartByPropertyId(propertyEntity.getPropertyId());
										if(cartEntityList!=null && !cartEntityList.isEmpty()) {
											for(CartEntity cartEntity: cartEntityList) {
												cartEntity.setCartIsActive(false);
												
											}
											cartRepository.saveAll(cartEntityList);
											
										}
										propertyEntity.setPropertyIsActive(false);
										
									}
								}
								sellerEntity.setSellerPropertyId(propertyToBeDeletedForAdmin);
								if(sellerEntity.getSellerNotificationId()!=null && !sellerEntity.getSellerNotificationId().isEmpty()) {
									for(NotificationEntity notificationEntity:sellerEntity.getSellerNotificationId()) {
										notificationEntity.setNotificationIsActive(false);
									}
								}
							}
							
							sellerToBeDeletedEntity.getUserAdminId().setAdminSellerIds(sellerToBeDeleted);
						}
						
						if(sellerToBeDeletedEntity.getUserAdminId().getAdminNotificationIds()!=null && !sellerToBeDeletedEntity.getUserAdminId().getAdminNotificationIds().isEmpty()) {
							for(NotificationEntity notificationEntity: sellerToBeDeletedEntity.getUserAdminId().getAdminNotificationIds()) {
								notificationEntity.setNotificationIsActive(false);
							}
						}
						sellerToBeDeletedEntity.getUserAdminId().setAdminIsActive(false);
					}
					
					
					
					AdminEntity adminEntity = adminRepository.getAdminByAdminId(sellerToBeDeletedEntity.getUserSellerId().getSellerRightBy());
					if(adminEntity==null) {
						throw new ServiceException(SERVICEEXCEPTION_NOADMIN);
					}
					userEntity = userRepository.save(sellerToBeDeletedEntity);
					
					adminEntity.setAdminSellerCount(adminEntity.getAdminSellerCount().subtract(BigInteger.valueOf(1l)));
					adminRepository.save(adminEntity);
					
					UsersEntity userEntityForAdmin = userRepository.findUserFromAdminId(adminEntity.getAdminId());
					if(userEntityForAdmin==null) {
						throw new ServiceException(SERVICEEXCEPTION_INVALIDADMIN);
					}
					
					
					sellerToBeReturned.setSellerId(userEntity.getUserSellerId().getSellerId());
					sellerToBeReturned.setSellerIsActive(userEntity.getUserSellerId().isSellerIsActive());
					sellerToBeReturned.setSellerRightBy(userEntity.getUserSellerId().getSellerRightBy());
					userToBeReturned.setUserId(userEntity.getUserId());
					userToBeReturned.setUserSellerId(sellerToBeReturned);
					
					NotificationEntity notificationEntityToAdmin = new NotificationEntity();
					notificationEntityToAdmin.setNotificationTo(userEntityForAdmin.getUserId());
					notificationEntityToAdmin.setNotificationContent("One of your seller "+sellerToBeDeletedEntity.getUserSellerId().getSellerCompanyName()+DELETESELLERNOTIFICATIONTOADMIN);
					notificationEntityToAdmin.setNotificationIsActive(true);
					notificationEntityToAdmin.setNotificationNotifiedTime(LocalDateTime.now());
					notificationEntityToAdmin.setNotificationViewedTime(LocalDateTime.now());
					notificationRepository.save(notificationEntityToAdmin);
					
					NotificationEntity notificationEntityToSeller = new NotificationEntity();
					notificationEntityToSeller.setNotificationTo(sellerToBeDeletedEntity.getUserId());
					notificationEntityToSeller.setNotificationContent(DELETESELLERNOTIFICATIONTOSELLER);
					notificationEntityToSeller.setNotificationIsActive(true);
					notificationEntityToSeller.setNotificationNotifiedTime(LocalDateTime.now());
					notificationEntityToSeller.setNotificationViewedTime(LocalDateTime.now());
					notificationRepository.save(notificationEntityToSeller);
					
					MailModel mailmodel=new MailModel();
					mailmodel.setMailto(userEntityForAdmin.getUserEmailId());
					mailmodel.setBody(notificationEntityToAdmin.getNotificationContent());
					mailmodel.setSub(DELETESELLERSUBJECTTOADMIN);
					MailVerification mailVerification =new MailVerification();
					mailVerification.sentmail(mailmodel);
					
					sellerToBeDeletedEntity.getUserNotificationIds().add(notificationEntityToSeller);
					userEntityForAdmin.getUserNotificationIds().add(notificationEntityToAdmin);
					
					userRepository.save(userEntityForAdmin);
					
					return userToBeReturned;
					
			}
			//***** Catch for ServiceException # # #
			catch(ServiceException serviceException)
			{
				//***** Logging the ServiceException
				logg(serviceException.getMessage());
						
				//***** Throwing the ServiceException
				throw serviceException;
			}		
			//***** Catch for Exception # # #
			catch(Exception exception)
			{
				//***** Logging the Exception
				logg(exception.getMessage());
						
				//***** Throwing the Exception
				throw exception;
			}
			
					
	}
	
	
	@Override

	public User getSellerByProperty(AddPropertyUser property, String saltstring) throws Exception{

		
		try {
			Seller sellerToBeReturned = new Seller();
			User userToBeReturned = new User();
			LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
			if(saltstring==null) {
			throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
			}

			if(property.getUserid()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			UsersEntity userEntity = userRepository.findUserById(property.getUserid(),saltstring);
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			if(property.getReturnPropertyId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOPROPERTYID);
			}
			SellerEntity sellerEntity = sellerRepository.findSellerByPropertyId(property.getReturnPropertyId());
			

			if(sellerEntity==null) {
			throw new ServiceException(SERVICEEXCEPTION_NOSELLERFORPROPERTY);
			}

			UsersEntity returnUserEntity = userRepository.findUserFromSellerId(sellerEntity);
			if(returnUserEntity==null) {
			throw new ServiceException(SERVICEEXCEPTION_NOUSERFORSELLER);
			}

			if(returnUserEntity.getUserAdminId()!=null)
				userToBeReturned.setUserAdminId(loginservice.adminForUser(returnUserEntity));
			if(returnUserEntity.getUserImage()!=null)
				userToBeReturned.setUserImage(loginservice.imageForUser(returnUserEntity.getUserImage()));
			userToBeReturned.setUserEmailId(returnUserEntity.getUserEmailId());
			userToBeReturned.setUserContactNo(returnUserEntity.getUserContactNo());
			userToBeReturned.setUserFirstName(returnUserEntity.getUserFirstName());
			userToBeReturned.setUserLastName(returnUserEntity.getUserLastName());
			userToBeReturned.setUserId(returnUserEntity.getUserId());
			userToBeReturned.setUserVerifed(returnUserEntity.isUserVerifed());
			userToBeReturned.setUserIsSeller(returnUserEntity.isUserIsSeller());
			userToBeReturned.setUserIsActive(returnUserEntity.isUserIsActive());
			userToBeReturned.setUserCreatedTime(returnUserEntity.getUserCreatedTime());
			userToBeReturned.setUserModifiedTime(returnUserEntity.getUserModifiedTime());


			sellerToBeReturned.setSellerCompanyName(sellerEntity.getSellerCompanyName());
			sellerToBeReturned.setSellerId(sellerEntity.getSellerId());
			sellerToBeReturned.setSellerPrivateContact(sellerEntity.getSellerPrivateContact());
			sellerToBeReturned.setSellerPropertyCount(sellerEntity.getSellerPropertyCount());
			sellerToBeReturned.setSellerRightBy(sellerEntity.getSellerRightBy());
			sellerToBeReturned.setSellerCreatedTime(sellerEntity.getSellerCreatedTime());
			if((sellerEntity.getSellerCompanyLogoId())!=null)
				sellerToBeReturned.setSellerCompanyLogoId(loginservice.imageForUser(sellerEntity.getSellerCompanyLogoId()));
			if((sellerEntity.getSellerFeedbackId())!=null)
				sellerToBeReturned.setSellerFeedbackIds(loginservice.feedback(sellerEntity.getSellerFeedbackId()));
			if((sellerEntity.getSellerNotificationId())!=null)
				sellerToBeReturned.setSellerNotificationIds(loginservice.notificationlist(sellerEntity.getSellerNotificationId()));
			if((sellerEntity.getSellerPropertyId())!=null)
				sellerToBeReturned.setSellerPropertyIds(loginservice.propertysetting(sellerEntity.getSellerPropertyId()));


			userToBeReturned.setUserSellerId(sellerToBeReturned);
			
			PropertyEntity propertyEntity = propertyRepository.getPropertyByPropertyId(property.getReturnPropertyId());
			if(propertyEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
			}
			if(sellerEntity.getSellerRightBy()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOADMIN);
			}
			UsersEntity userEntityForAdmin = userRepository.findUserFromAdminId(sellerEntity.getSellerRightBy());
			if(userEntityForAdmin==null) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDADMIN);
			}
			userToBeReturned.setContactPersonName(userEntityForAdmin.getUserFirstName()+" "+userEntityForAdmin.getUserLastName());
			userToBeReturned.setContactNumber(userEntityForAdmin.getUserSellerId().getSellerPrivateContact());

			NotificationEntity notificationEntity = new NotificationEntity();
			notificationEntity.setNotificationTo(returnUserEntity.getUserId());
			notificationEntity.setNotificationContent(CONTACTSELLERNOTIFICATIONTOSELLER);
			notificationEntity.setNotificationIsActive(true);
			notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
			notificationEntity.setNotificationViewedTime(LocalDateTime.now());
			notificationRepository.save(notificationEntity);
			
			MailModel mailmodel=new MailModel();
			mailmodel.setMailto(returnUserEntity.getUserEmailId());
			mailmodel.setBody(notificationEntity.getNotificationContent());
			mailmodel.setSub(CONTACTSELLERSUBJECTTOSELLER);
			MailVerification mailVerification =new MailVerification();
			mailVerification.sentmail(mailmodel);
			
			

			NotificationEntity notificationEntityToAdmin = new NotificationEntity();
			notificationEntityToAdmin.setNotificationTo(userEntityForAdmin.getUserId());
			notificationEntityToAdmin.setNotificationContent(userEntity.getUserFirstName()+" "+userEntity.getUserLastName()+CONTACTSELLERNOTIFICATION+propertyEntity.getPropertyLandmark()+", "+propertyEntity.getPropertyCity()+", "+propertyEntity.getPropertyState()+", "+propertyEntity.getPropertyCountry()+". Contact seller at "+sellerEntity.getSellerPrivateContact()+". Contact the viewer at "+userEntity.getUserContactNo());
			notificationEntityToAdmin.setNotificationIsActive(true);
			notificationEntityToAdmin.setNotificationNotifiedTime(LocalDateTime.now());
			notificationEntityToAdmin.setNotificationViewedTime(LocalDateTime.now());
			notificationRepository.save(notificationEntityToAdmin);
			
			mailmodel.setMailto(userEntityForAdmin.getUserEmailId());
			mailmodel.setBody(notificationEntityToAdmin.getNotificationContent());
			mailmodel.setSub(CONTACTSELLERSUBJECTTOADMIN);
			mailVerification.sentmail(mailmodel);
			

			returnUserEntity.getUserNotificationIds().add(notificationEntity);
			userEntityForAdmin.getUserNotificationIds().add(notificationEntityToAdmin);
			userRepository.save(returnUserEntity);


			return userToBeReturned;


		}
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
					
			//***** Throwing the ServiceException
			throw serviceException;
		}		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
					
			//***** Throwing the Exception
			throw exception;
		}
	}
	
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	//***** Declaring the Logger class # # #
	private void logg(String message) {
		log.error(message);
	}

}