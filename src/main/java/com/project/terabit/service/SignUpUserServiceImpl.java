package com.project.terabit.service;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Properties;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.controller.ControllerException;
import com.project.terabit.entity.CartEntity;
import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.ImageHandlerModel;
import com.project.terabit.model.MailModel;
import com.project.terabit.model.User;
import com.project.terabit.repository.CartRepository;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.utility.MailVerification;
import com.project.terabit.utility.PasswordHashing;
import com.project.terabit.utility.RandomStringGenerator;
import com.project.terabit.validator.DeleteUserValidator;
import com.project.terabit.validator.LoginUserValidator;
import com.project.terabit.validator.SignUpUserValidator;
import com.project.terabit.validator.UpdateUserValidator;


/**@author deepik sriram & joshua shakespeare & solai ganesh
/**
 * The Class SignUpServiceImpl.
 */
@Service
public class SignUpUserServiceImpl implements SignUpUserService{
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());

	/** The user repository. */
	@Autowired
	private UserRepository userRepository;
	
	/** The seller repository. */
	@Autowired
	private SellerRepository sellerRepository;
	
	@Autowired
	CartRepository cartRepository;
	
	/** The password hasher. */
	PasswordHashing passwordHasher = new PasswordHashing();
	
	RandomStringGenerator saltstring = new RandomStringGenerator();
	

	

	
	
	/** The Constant SERVICEEXCEPTION_USERIDEXCEPTION. */
	private static final String SERVICEEXCEPTION_USERIDEXCEPTION = "UTILITYSERVICE.invalid_user";
	
	private static final String SERVICEEXCEPTION_PASSKEYMISSMATCHEXCEPTION = "UPDATESERVICE.invalid_password";
	
	private static final String SERVICEEXCEPTION_SALTSTRINGMISSMATCH = "UTILITYSERVICE.invalid_saltstring";
	
    private static final String SERVICEEXCEPTION_URLMISSMATCH="UTILITYSERVICE.saltstring_not_provided";
	private static final String SERVICEEXCEPTION_NOUSERID = "UTILITYSERVICE.no_userid_provided";
	
	
	/** The Constant userAlreadyExists. */
	private static final String SERVICEEXCEPTION_USERALREADYEXISTS = "SIGNUPSERVICE.invalid_signUp";
	

	
	/** The Constant userAlreadyExists. */
	private static final String SERVICEEXCEPTION_CONTACTNUMBERALREADYEXISTS = "UPDATESERVICE.invalid_contactNumber";
	
	/** The Constant userAlreadyExists. */
	private static final String SERVICEEXCEPTION_EMAILALREADYEXISTS = "UPDATESERVICE.invalid_mailId";
	
	private static final String SERVICEEXCEPTION_PASSKEYVALIDATIONFAILED = "UPDATESERVICE.invalid_passkey_validation";
	
	private static final String SERVICEEXCEPTION_NOEMAILANDPHONE="SERVICEEXCEPTION.no_mail_or_contactno_provided";

	private static final String WELCOMENOTIFICATION = "Welcome to Terabit!";
	
	private static final String PASSKEYCHANGE = "[TeraBit] Please reset your Password";
	
	private static final String SERVICEEXCEPTIONSAMEPASSKEY="CHANGEPASSWORD.password__is_same_as_oldone";
	

	
	private static final String SERVICEEXCEPTIONNOAUTHORIZATIONID="CHANGEPASSWORD.authorization_id_is_Empty";
	
	/** The Constant userAlreadyExists. */
	
	private static final String SERVICEEXCEPTIONNOPASSKEY="CHANGEPASSWORD.password_id_is_Empty";

	private static final String SERVICEEXCEPTION_VERIFICATIONTIMEOUT="SERVICE.User_Cannot_Be_Verified";
	
	private static final String SERVICEEXCEPTION_NOSELLERID="DELETESERVICE.No_seller_for_given_id";
	
	private static final String USERVERIFICATIONLINK="MAILLINK.user_verification";

	
	private static final String PASSKEYFORGOTLINK="MAILLINK.forgot_password";

	
	private static final String USERVERIFICATIONBODY = "Kindly click on the below verification link to get verified.";
	
	private static final String FORGETPASSKEYSUBJECT = "Please click on the below link to change your password";

	/** The property. */
	//****Initializing:		Property # # #
	Properties prop =new Properties();						
	
	/** The input stream. */
	//*****Initializing and populating inputstream with application.properties
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");	
	
	/* (non-Javadoc)
	 * @see com.project.terabit.service.SignUpService#createUser(com.project.terabit.model.User)
	 */
	@Override
	public User saveUser(User user) throws Exception {
		UsersEntity userEntityToBeAdded=new UsersEntity();
		UsersEntity userEntityToBeReturned;
		try {
		SignUpUserValidator.validate(user);
		UsersEntity userEntity = userRepository.checkUser(user.getUserEmailId(),user.getUserContactNo());
		if(userEntity!=null) {
			throw new ServiceException(SERVICEEXCEPTION_USERALREADYEXISTS);
		}
		userEntityToBeAdded.setIsuserAuthenticated(user.isUserAuthenticated());
		userEntityToBeAdded.setUserPassword(passwordHasher.hashing(user.getUserPassword()));
		userEntityToBeAdded.setUserContactNo(user.getUserContactNo());
		userEntityToBeAdded.setUserCreatedTime(LocalDateTime.now());
		userEntityToBeAdded.setUserEmailId(user.getUserEmailId());
		userEntityToBeAdded.setUserFirstName(user.getUserFirstName());
		userEntityToBeAdded.setUserIsActive(true);
		userEntityToBeAdded.setUserIsSeller(user.isUserIsSeller());
		userEntityToBeAdded.setUserLastName(user.getUserLastName());
		userEntityToBeAdded.setUserModifiedTime(LocalDateTime.now());
		userEntityToBeAdded.setSaltString(saltstring.randomString());
		userEntityToBeAdded.setUserPin(user.getUserPin());
		userEntityToBeAdded.setUserVerifed(user.isUserVerifed());
		userEntityToBeAdded.setAuthorizationid(RandomStringGenerator.authorizationIdGenerator());
		userEntityToBeReturned=userRepository.save(userEntityToBeAdded);
		log.info("user id generated: "+userEntityToBeReturned.getUserId());
		
		NotificationEntity notificationEntity = new NotificationEntity();
		notificationEntity.setNotificationTo(userEntityToBeReturned.getUserId());
		notificationEntity.setNotificationContent(WELCOMENOTIFICATION);
		notificationEntity.setNotificationIsActive(true);
		notificationEntity.setNotificationNotifiedTime(LocalDateTime.now());
		notificationEntity.setNotificationViewedTime(LocalDateTime.now());
		log.info("notification content  "+WELCOMENOTIFICATION);
		userEntityToBeAdded.getUserNotificationIds().add(notificationEntity);
		userRepository.save(userEntityToBeAdded);
		
		MailModel mailmodel=new MailModel();
		prop.load(inputStream);
		mailmodel.setLink(prop.getProperty(USERVERIFICATIONLINK)+ userEntityToBeAdded.getAuthorizationid());
		mailmodel.setMailto(userEntityToBeAdded.getUserEmailId());
		mailmodel.setBody("Dear "+userEntityToBeAdded.getUserFirstName()+" "+userEntityToBeAdded.getUserLastName()+","+"<br>"+USERVERIFICATIONBODY);
		mailmodel.setSub(WELCOMENOTIFICATION);
		mailmodel.setOtp(1234);
		MailVerification mailVerification =new MailVerification();
		mailVerification.sentmail(mailmodel);
		
		user.setUserIsActive(true);
		user.setUserId(userEntityToBeReturned.getUserId());
		user.setUserCreatedTime(LocalDateTime.now());
		} 
		catch(ServiceException exception){
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("SignUp"+exception.getMessage());
			throw exception;
		}
		return user;
		
	}

	/* (non-Javadoc)
	 * @see com.project.terabit.service.SignUpService#verifyUser(com.project.terabit.entity.UserEntity)
	 */
	@Override
	public User updateUser(User user, String saltstring) throws Exception {
		
		try {
			UsersEntity userEntityToBeUpdated;
			LocalDateTime currenttime=LocalDateTime.now();
			if(user.getUserId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
			}
			log.info("user id: "+user.getUserId());
			if(saltstring==null) {
				throw new ServiceException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			log.info("saltstring: "+saltstring);
			userEntityToBeUpdated=userRepository.findUserByUserId(user.getUserId());
			if(userEntityToBeUpdated==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}else if(!userEntityToBeUpdated.getSaltString().equals(saltstring)) {
				throw new ServiceException(SERVICEEXCEPTION_SALTSTRINGMISSMATCH);
			}
			
			if(user.getUserEmailId()!=null) {
				log.info("mail id: "+user.getUserEmailId());
				UsersEntity userEntityForMail = userRepository.checkUserMailId(user.getUserEmailId());
				if((userEntityForMail!=null) && !(userEntityForMail.getUserId().toString().equals(user.getUserId().toString())) ) {
					throw new ServiceException(SERVICEEXCEPTION_EMAILALREADYEXISTS);
				}

			}
			if(user.getUserContactNo()!=null) {
				log.info("contact number: "+user.getUserContactNo());
				UsersEntity userEntityForPhone = userRepository.checkUserContactNumber(user.getUserContactNo());
				if((userEntityForPhone!=null)  && !(userEntityForPhone.getUserId().toString().equals(user.getUserId().toString()))) {
					throw new ServiceException(SERVICEEXCEPTION_CONTACTNUMBERALREADYEXISTS);
				}
			}
			if(user.getUserPassword()!=null && user.getNewUserPassword()!=null) {
				log.info("password is passed");
				if(userEntityToBeUpdated.getUserPassword().equals(passwordHasher.hashing(user.getUserPassword()))) {
					Boolean flag = true;
					if(!(flag.equals(LoginUserValidator.validatePassword(user.getNewUserPassword())))) {
						throw new ServiceException(SERVICEEXCEPTION_PASSKEYVALIDATIONFAILED);
					}
					
					userEntityToBeUpdated.setUserPassword(passwordHasher.hashing(user.getNewUserPassword()));
				}
				else {
					throw new ServiceException(SERVICEEXCEPTION_PASSKEYMISSMATCHEXCEPTION);
				}
			}
			
			if(user.getUserEmailId()==null) user.setUserEmailId(userEntityToBeUpdated.getUserEmailId());
			if(user.getUserContactNo()==null) user.setUserContactNo(userEntityToBeUpdated.getUserContactNo());
			if(user.getUserFirstName()==null) {
				user.setUserFirstName(userEntityToBeUpdated.getUserFirstName());
			}else log.info("first name: "+user.getUserFirstName());
			if(user.getUserLastName()==null) {
			    user.setUserLastName(userEntityToBeUpdated.getUserLastName());
			}else log.info("last name: "+user.getUserLastName());
			
			UpdateUserValidator.updateInfoValidate(user);

			
			
			userEntityToBeUpdated.setUserAdminId(userEntityToBeUpdated.getUserAdminId());
			userEntityToBeUpdated.setIsuserAuthenticated(userEntityToBeUpdated.isIsuserAuthenticated());
			userEntityToBeUpdated.setUserCarts(userEntityToBeUpdated.getUserCarts());
			
			userEntityToBeUpdated.setUserCreatedTime(userEntityToBeUpdated.getUserCreatedTime());
			
			userEntityToBeUpdated.setUserFeedbacks(userEntityToBeUpdated.getUserFeedbacks());
			if(user.getUserFirstName()!=null) {
				userEntityToBeUpdated.setUserFirstName(user.getUserFirstName());
			}
			if(user.getUserLastName()!=null) {
				userEntityToBeUpdated.setUserLastName(user.getUserLastName());
			}
			if(user.getUserContactNo()!=null) {
				userEntityToBeUpdated.setUserContactNo(user.getUserContactNo());
			}
			if(user.getUserEmailId()!=null) {
				userEntityToBeUpdated.setUserEmailId(user.getUserEmailId());
			}
			userEntityToBeUpdated.setUserImage(userEntityToBeUpdated.getUserImage());
			userEntityToBeUpdated.setUserIsActive(userEntityToBeUpdated.isUserIsActive());
			userEntityToBeUpdated.setUserIsSeller(userEntityToBeUpdated.isUserIsSeller());
			
			userEntityToBeUpdated.setUserModifiedTime(currenttime);
			userEntityToBeUpdated.setUserNotificationIds(userEntityToBeUpdated.getUserNotificationIds());
			userEntityToBeUpdated.setUserPin(userEntityToBeUpdated.getUserPin());
			userEntityToBeUpdated.setUserSellerId(userEntityToBeUpdated.getUserSellerId());
			userEntityToBeUpdated.setUserVerifed(userEntityToBeUpdated.isUserVerifed());
			userEntityToBeUpdated.setUserViewedProperties(userEntityToBeUpdated.getUserViewedProperties());
			userEntityToBeUpdated.setUserPassword(userEntityToBeUpdated.getUserPassword());
			userRepository.save(userEntityToBeUpdated);
			
			user.setUserVerifed(userEntityToBeUpdated.isUserVerifed());
			user.setUserAuthenticated(userEntityToBeUpdated.isIsuserAuthenticated());
			user.setSaltstring(userEntityToBeUpdated.getSaltString());
			user.setUserIsSeller(userEntityToBeUpdated.isUserIsSeller());
			user.setUserIsActive(userEntityToBeUpdated.isUserIsActive());
			user.setUserCreatedTime(userEntityToBeUpdated.getUserCreatedTime());
			user.setUserModifiedTime(userEntityToBeUpdated.getUserModifiedTime());
			LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
			if(userEntityToBeUpdated.getUserCarts()!=null) {
				user.setUserCarts(loginservice.cartForUser(userEntityToBeUpdated.getUserCarts()));
			}
			if(userEntityToBeUpdated.getUserFeedbacks()!=null) {
				user.setUserFeedbacks(loginservice.feedback(userEntityToBeUpdated.getUserFeedbacks()));
			}
			if(userEntityToBeUpdated.getUserImage()!=null) {
				user.setUserImage(loginservice.imageForUser(userEntityToBeUpdated.getUserImage()));
			}
			if(userEntityToBeUpdated.getUserAdminId()!=null) {
				user.setUserAdminId(loginservice.adminForUser(userEntityToBeUpdated));
			}
			if(userEntityToBeUpdated.getUserSellerId()!=null) {
				user.setUserSellerId(loginservice.sellerForAdmin(userEntityToBeUpdated.getUserSellerId()));
			}
			if(userEntityToBeUpdated.getUserNotificationIds()!=null) {
				user.setUserNotificationIds(loginservice.notificationlist(userEntityToBeUpdated.getUserNotificationIds()));
			}
			if(userEntityToBeUpdated.getUserViewedProperties()!=null) {
				user.setUserViewedProperties(loginservice.viewedproperty(userEntityToBeUpdated.getUserViewedProperties(), userEntityToBeUpdated.getUserId()));
			}
			
			log.info("output user id: "+userEntityToBeUpdated.getUserId());
			
			user.setUserPassword(null);
			user.setNewUserPassword(null);
		
			return user;
		}catch(ServiceException exception) {
			logg("Update:"+exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg(exception.getMessage());
			throw exception;
		}
		
		
	}
	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User deleteUser(String saltstring,User user) throws Exception{
		UsersEntity userEntity = new UsersEntity();
		try {
			if(user.getUserId()==null) {
				throw new ControllerException(SERVICEEXCEPTION_NOUSERID);
			}
			log.info("user id: "+user.getUserId());
			if(saltstring==null){
				throw new ControllerException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			log.info("saltstring: "+saltstring);
			DeleteUserValidator.validate(user.getUserId().toString());
			userEntity = userRepository.findUserByUserId(user.getUserId());
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
			}else if(!userEntity.getSaltString().equals(saltstring)) {
				throw new ServiceException(SERVICEEXCEPTION_SALTSTRINGMISSMATCH);
			}
			userEntity.setUserIsActive(false);
			if((userEntity.isUserIsSeller()) && (userEntity.getUserSellerId()!=null)) {
				log.info("user is a seller");
				SellerEntity sellerEntity = userEntity.getUserSellerId();
				if(sellerEntity==null) {
					throw new ServiceException(SERVICEEXCEPTION_NOSELLERID);
				}
				log.info("seller id: "+sellerEntity.getSellerId());
				sellerEntity.setSellerIsActive(false);
				if(sellerEntity.getSellerPropertyId()!=null && !sellerEntity.getSellerPropertyId().isEmpty()) {
					for(int index=0;index<sellerEntity.getSellerPropertyId().size();index++) {
						ImageHandlerServiceImpl imagehandlerservice = new ImageHandlerServiceImpl();
						if(sellerEntity.getSellerPropertyId().get(index).getPropertyImageId()!=null && !sellerEntity.getSellerPropertyId().get(index).getPropertyImageId().isEmpty()) {
							ImageHandlerModel imageHandler = new ImageHandlerModel();
							imageHandler.setUserId(userEntity.getUserId().toString());
							imageHandler.setPropertyId(sellerEntity.getSellerPropertyId().get(index).getPropertyId());
							imageHandler.setSellerId(sellerEntity.getSellerId());
							for(ImageEntity imageEntity: sellerEntity.getSellerPropertyId().get(index).getPropertyImageId()) {
								imageHandler.setImageId(imageEntity.getImageId());
								imagehandlerservice.deleteImage(imageHandler, userEntity.getSaltString());
							}
						}
						sellerEntity.getSellerPropertyId().get(index).setPropertyIsActive(false);
					}
				}
			}
			if((userEntity.getUserAdminId()!=null) && (userEntity.getUserAdminId().getAdminRightsBy()!=null) && userEntity.getUserAdminId().isAdminIsActive()) {
				log.info("user is an admin");
				log.info("admin id: "+userEntity.getUserAdminId());
				List<SellerEntity> sellerToBeDeleted = sellerRepository.getSellerByAdmin(userEntity.getUserAdminId().getAdminId());
				if(sellerToBeDeleted!=null && !sellerToBeDeleted.isEmpty()) {
					for(SellerEntity sellerEntity: sellerToBeDeleted) {
						sellerEntity.setSellerIsActive(false);
						UsersEntity userEntityForSeller = userRepository.findUserFromSellerId(sellerEntity.getSellerId());
						List<PropertyEntity> propertyToBeDeleted = sellerEntity.getSellerPropertyId();
						if(propertyToBeDeleted!=null && !propertyToBeDeleted.isEmpty()) {
							for(PropertyEntity propertyEntity: propertyToBeDeleted) {
								ImageHandlerServiceImpl imagehandlerservice = new ImageHandlerServiceImpl();
								if(propertyEntity.getPropertyImageId()!=null && !propertyEntity.getPropertyImageId().isEmpty()) {
									ImageHandlerModel imageHandler = new ImageHandlerModel();
									imageHandler.setUserId(userEntityForSeller.getUserId().toString());
									imageHandler.setPropertyId(propertyEntity.getPropertyId());
									imageHandler.setSellerId(sellerEntity.getSellerId());
									for (ImageEntity imageEntity : propertyEntity.getPropertyImageId()) {
										imageHandler.setImageId(imageEntity.getImageId());
										imagehandlerservice.deleteImage(imageHandler, userEntityForSeller.getSaltString());
									}
								}
								List<CartEntity> cartEntityList = cartRepository.findCartByPropertyId(propertyEntity.getPropertyId());
								if(cartEntityList!=null && !cartEntityList.isEmpty()) {
									for(CartEntity cartEntity: cartEntityList) {
										cartEntity.setCartIsActive(false);
										
									}
									cartRepository.saveAll(cartEntityList);
									
								}
								propertyEntity.setPropertyIsActive(false);
							}
						}
						sellerEntity.setSellerPropertyId(propertyToBeDeleted);
						if(sellerEntity.getSellerNotificationId()!=null && !sellerEntity.getSellerNotificationId().isEmpty()) {
							for(NotificationEntity notificationEntity:sellerEntity.getSellerNotificationId()) {
								notificationEntity.setNotificationIsActive(false);
							}
						}
					}
					
					userEntity.getUserAdminId().setAdminSellerIds(sellerToBeDeleted);
				}
				
				if(userEntity.getUserAdminId().getAdminNotificationIds()!=null && !userEntity.getUserAdminId().getAdminNotificationIds().isEmpty()) {
					for(NotificationEntity notificationEntity: userEntity.getUserAdminId().getAdminNotificationIds()) {
						notificationEntity.setNotificationIsActive(false);
					}
				}
				userEntity.getUserAdminId().setAdminIsActive(false);

			}
			
			if(userEntity.getUserNotificationIds()!=null && !userEntity.getUserNotificationIds().isEmpty()) {
				for(NotificationEntity notificationEntity : userEntity.getUserNotificationIds()) {
					notificationEntity.setNotificationIsActive(false);
				}
			}
			
			userRepository.save(userEntity);
			return user;
		}catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("deleteUser "+exception.getMessage());
			throw exception;
		}
	}
	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public void forgetPassword(User user) throws Exception
	{
		try {
		if(user.getUserEmailId()==null )
			throw new ServiceException(SERVICEEXCEPTION_NOEMAILANDPHONE);
		if(user.getNewUserPassword()==null) {
			throw new ServiceException(SERVICEEXCEPTIONNOPASSKEY);
		}
		UsersEntity passwordToBeChangeUserEntity=userRepository.findUserByEmailID(user.getUserEmailId());
		
		
		if(passwordToBeChangeUserEntity==null)
			throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
		
		
		passwordToBeChangeUserEntity.setUserModifiedTime(LocalDateTime.now());
		
		passwordToBeChangeUserEntity.setAuthorizationid(RandomStringGenerator.authorizationIdGenerator());
		
		userRepository.save(passwordToBeChangeUserEntity);
		MailModel mailmodel=new MailModel();
		prop.load(inputStream);
		mailmodel.setLink(prop.getProperty(PASSKEYFORGOTLINK)+ user.getNewUserPassword()+"?authorizationid="+passwordToBeChangeUserEntity.getAuthorizationid());
		mailmodel.setMailto(passwordToBeChangeUserEntity.getUserEmailId());
		mailmodel.setBody("Dear "+passwordToBeChangeUserEntity.getUserFirstName()+" "+passwordToBeChangeUserEntity.getUserLastName()+","+"<br>"+FORGETPASSKEYSUBJECT);
		mailmodel.setSub(PASSKEYCHANGE);
		
		
		MailVerification mailVerification =new MailVerification();
		mailVerification.sentmail(mailmodel);
		
		
		}
		catch(ServiceException serviceexception)
		{
			logg(serviceexception.getMessage());
			
			throw serviceexception;
			
		}
		catch(Exception exception)
		{
			logg(exception.getMessage());
			
			throw exception;
		}
	}
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User changePassword(String authorizationid, String passkey) throws Exception {
			try
			{
				User user = new User();
				if(authorizationid==null)
				{
					throw new ServiceException(SERVICEEXCEPTIONNOAUTHORIZATIONID);
				}
				log.info(authorizationid);
				if(passkey==null)
				{
					throw new ServiceException(SERVICEEXCEPTIONNOPASSKEY);
				}
				UsersEntity passwordToBeChangeUserEntity=userRepository.findUserFromAuthorizationId(authorizationid);
				
				if(passwordToBeChangeUserEntity==null)
					throw new ServiceException(SERVICEEXCEPTION_USERIDEXCEPTION);
				
				if(passwordToBeChangeUserEntity.getUserModifiedTime().compareTo(LocalDateTime.now().minusMinutes(15))<0)
				{
					throw new ServiceException(SERVICEEXCEPTION_VERIFICATIONTIMEOUT);
				}
				
				
				
				if(passwordHasher.hashing(passkey).equals(passwordToBeChangeUserEntity.getUserPassword()))
					throw new ServiceException(SERVICEEXCEPTIONSAMEPASSKEY);
				passwordToBeChangeUserEntity.setUserPassword(passwordHasher.hashing(passkey));
				
				userRepository.save(passwordToBeChangeUserEntity);
				
				user.setUserVerifed(passwordToBeChangeUserEntity.isUserVerifed());
				user.setUserAuthenticated(passwordToBeChangeUserEntity.isIsuserAuthenticated());
				user.setSaltstring(passwordToBeChangeUserEntity.getSaltString());
				user.setUserIsSeller(passwordToBeChangeUserEntity.isUserIsSeller());
				user.setUserIsActive(passwordToBeChangeUserEntity.isUserIsActive());
				user.setUserCreatedTime(passwordToBeChangeUserEntity.getUserCreatedTime());
				user.setUserModifiedTime(passwordToBeChangeUserEntity.getUserModifiedTime());
				LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
				if(passwordToBeChangeUserEntity.getUserCarts()!=null) {
					user.setUserCarts(loginservice.cartForUser(passwordToBeChangeUserEntity.getUserCarts()));
				}
				if(passwordToBeChangeUserEntity.getUserFeedbacks()!=null) {
					user.setUserFeedbacks(loginservice.feedback(passwordToBeChangeUserEntity.getUserFeedbacks()));
				}
				if(passwordToBeChangeUserEntity.getUserImage()!=null) {
					user.setUserImage(loginservice.imageForUser(passwordToBeChangeUserEntity.getUserImage()));
				}
				if(passwordToBeChangeUserEntity.getUserAdminId()!=null) {
					user.setUserAdminId(loginservice.adminForUser(passwordToBeChangeUserEntity));
				}
				if(passwordToBeChangeUserEntity.getUserSellerId()!=null) {
					user.setUserSellerId(loginservice.sellerForAdmin(passwordToBeChangeUserEntity.getUserSellerId()));
				}
				if(passwordToBeChangeUserEntity.getUserNotificationIds()!=null) {
					user.setUserNotificationIds(loginservice.notificationlist(passwordToBeChangeUserEntity.getUserNotificationIds()));
				}
				if(passwordToBeChangeUserEntity.getUserViewedProperties()!=null) {
					user.setUserViewedProperties(loginservice.viewedproperty(passwordToBeChangeUserEntity.getUserViewedProperties(), passwordToBeChangeUserEntity.getUserId()));
				}
				
				
				return user;
			}
			catch(ServiceException serviceexception)
			{
				logg(serviceexception.getMessage());
				
				throw serviceexception;
				
			}
			catch(Exception exception)
			{
				logg(exception.getMessage());
				
				throw exception;
			}
			
		
	}
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}


	
	
}
