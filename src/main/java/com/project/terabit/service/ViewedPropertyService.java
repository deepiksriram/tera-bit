package com.project.terabit.service;

import java.util.List;
import java.util.UUID;

import com.project.terabit.model.Property;
import com.project.terabit.model.ViewedProperty;

public interface ViewedPropertyService {

	public Property createViewedProperty(ViewedProperty viewedProperty,String saltString) throws Exception;
	
	public List<Property> getViewedPropertyForUser(UUID userId, String saltString) throws Exception;

}
