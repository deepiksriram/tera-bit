package com.project.terabit.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.project.terabit.model.AddPropertyUser;
import com.project.terabit.model.Property;
import com.project.terabit.model.User;

// TODO: Auto-generated Javadoc
/**
 * The Interface AddPropertyBySeller.
 */
@Component
public interface PropertyService {
	
	/**
	 * Adds the property by seller.
	 *
	 * @param userproperty the userproperty
	 * @param saltstring the saltstring
	 * @return the list
	 * @throws Exception the exception
	 */
	public String addPropertyBySeller(AddPropertyUser userproperty,String saltstring) throws Exception;
	
	/**
	 * Adds the property by seller.
	 *
	 * @param userproperty the userproperty
	 * @param saltstring the saltstring
	 * @return the list
	 * @throws Exception the exception
	 */
	public String updatePropertyBySeller(AddPropertyUser userproperty,String saltstring) throws Exception;
	
	public List<Property> getPropertyBySeller(User user,String saltstring) throws Exception;
	
	public String deletePropertyBySeller(AddPropertyUser addPropertyUser,String saltstring) throws Exception;
	
}
