package com.project.terabit.service;


import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.web.multipart.MultipartFile;

import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Image;
import com.project.terabit.model.ImageHandlerModel;
import com.project.terabit.repository.ImageRepository;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.UserRepository;

@Service
@Transactional(readOnly=true)
public class ImageHandlerServiceImpl implements ImageHandlerService{
	
	@Autowired 
	ImageRepository imagerepository;
	
	@Autowired
	PropertyRepository propertyrepository;
	
	@Autowired
	UserRepository userrepository;
	
	/** The Constant SERVICEEXCEPTION_NOUSER. */
	//***** Appropriate message to be thrown in each case craetion which is added in the application property controller Exception
	private static final String SERVICEEXCEPTION_NOIMAGEOBJ = "IMAGEHANDLERSERVICE.no_image_object";
		
	/** The Constant SERIVCEEXCEPTION_NOUSERID. */
	private static final String SERVICEEXCEPTION_NOUSERID = "IMAGEHANDLERSERVICE.no_image_user_id";
	
	/** The Constant NOIMAGE_PROVIDED. */
	//***** Message related to Service Exception
	private static final String SERVICEEXCEPTION_NOIMAGE_PROVIDED = "IMAGEHANDLERSERVICE.no_image";
	
	/** The Constant ADMIN_IS_NOT_ACTIVE. */
	private static final String SERVICEEXCEPTION_NOSALTSTRING = "IMAGEHANDLERSERVICE.no_saltstring_provided";
	
	/** The Constant SERVICEEXCEPTION_IMAGEUSER_NOTEXIST. */
	private static final String SERVICEEXCEPTION_IMAGEUSER_NOTEXIST = "IMAGEHANDLERSERVICE.user_not_exist";
	
	/** The Constant SERVICEEXCEPTION_NOPROPERTY. */
	private static final String SERVICEEXCEPTION_NOPROPERTY = "IMAGEHANDLERSERVICE.no_property_exist";
	
	/** The Constant SERVICEEXCEPTION_NOIMAGEID. */
	private static final String SERVICEEXCEPTION_NOIMAGEID = "IMAGEHANDLERSERVICE.no_imageid_provided";
	
	/** The Constant SERVICEEXCEPTION_NOIMAGEEXIST. */
	private static final String SERVICEEXCEPTION_NOIMAGEFOUND = "IMAGEHANDLERSERVICE.no_image_found";
	
	
	/** The Constant SERVICEEXCEPTION_NOIMAGEEXIST. */
	private static final String SERVICEEXCEPTION_NOTANSELLER = "IMAGEHANDLERSERVICE.no_user_is_not_a_seller";
	
	
	private static final String URL = "IMAGEHANDLERSERVICE.url";
	
	private static final String SERVICEEXCEPTION_IMAGEISNOTOWNED = "IMAGEHANDLERSERVICE.image_does_not_belong_to_the_user_or_seller";
	
	
	
	
	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The property. */
	//****Initializing:		Property # # #
	Properties prop =new Properties();						
	
	/** The input stream. */
	//*****Initializing and populating inputstream with application.properties
	InputStream	inputStream=getClass().getClassLoader().getResourceAsStream("application.properties");	

	
	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public Image saveImage(ImageHandlerModel imagetobesaved,String saltstring) throws ServiceException,Exception
	{

        String imagename;
        String urltobesaved;

        Image imagetobesavedreturn=new Image();
      
        imagename=imagetobesaved.getImagetobesaved().getOriginalFilename();
      
        try
        {
        	
        	if(imagetobesaved.getImagetobesaved().isEmpty()) {
        		throw new ServiceException(SERVICEEXCEPTION_NOIMAGE_PROVIDED);
        	}
        	
        	if(imagetobesaved.getUserId()==null) {
        		throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
        	}
        	log.info("userid: "+imagetobesaved.getUserId());
        	if(saltstring==null)
        	{
        		throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
        	}
        	log.info("saltstring: "+saltstring);
        	
        	UsersEntity userentity =userrepository.findUserById(UUID.fromString(imagetobesaved.getUserId()), saltstring);
        	
        	log.info("userentity: "+userentity);
        	
        	if(userentity==null) {
        		throw new ServiceException(SERVICEEXCEPTION_IMAGEUSER_NOTEXIST);
        	}
        	prop.load(inputStream); 
        	urltobesaved=prop.getProperty(URL)+"user_"+imagetobesaved.getUserId()+"/";
        	PropertyEntity propertyentity=new PropertyEntity();
        	if(imagetobesaved.getSellerId()!=null)
        	{	
        		if((userentity.getUserSellerId()!=null) && userentity.isUserIsSeller() &&userentity.getUserSellerId().isSellerIsActive()&&(userentity.getUserSellerId().getSellerId()).equals(imagetobesaved.getSellerId()))
        		{
        			urltobesaved=urltobesaved+"sellerid_"+imagetobesaved.getSellerId().toString()+"/";
        			if(imagetobesaved.getPropertyId()!=null)
                    {
        				//Getting the active property
                    	propertyentity=propertyrepository.getPropertyByPropertyId(imagetobesaved.getPropertyId());
                    	if((propertyentity==null)){
                    		throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
                    	}
                    	
                    	log.info("propertyentity: "+propertyentity);

                    	urltobesaved=urltobesaved+"property_"+imagetobesaved.getPropertyId()+"/";
                    }
        			
        		}
        		else {
        			
        			throw new ServiceException(SERVICEEXCEPTION_NOTANSELLER);
        			
        		}
        		
        	}
        	else {
        		imagename=userentity.getUserFirstName()+"'s-profile-picture"+"."+imagetobesaved.getImagetobesaved().getOriginalFilename().split("[.]")[1];
        		log.info("Profile picture name: "+imagename);
        	}
        	urltobesaved=urltobesaved+imagename;

        	
        	log.info("Image Url: "+urltobesaved);
        	
//  Commenting the server image storage    
        	
           ByteArrayInputStream fileinputstream=new ByteArrayInputStream(imagetobesaved.getImagetobesaved().getBytes());
     
           File outputfile=new File(urltobesaved);
           outputfile.mkdirs();
          
           ImageIO.write(ImageIO.read(fileinputstream), "jpg", outputfile);
        	  
            ImageEntity imagetobesavedentity=new ImageEntity();
            
            imagetobesavedentity.setImageDescription(Base64Utils.encodeToString(imagetobesaved.getImagetobesaved().getBytes()));
            imagetobesavedentity.setImageCreatedTime(LocalDateTime.now());
            imagetobesavedentity.setImageIsActive(true);
            imagetobesavedentity.setImagePath(urltobesaved);
            imagerepository.save(imagetobesavedentity);
            
            fileinputstream.close();
            
            imagetobesavedreturn.setImageCreatedTime(imagetobesavedentity.getImageCreatedTime());
            imagetobesavedreturn.setImageId(imagetobesavedentity.getImageId());
            imagetobesavedreturn.setImageIsActive(imagetobesavedentity.isImageIsActive());
            imagetobesavedreturn.setImagePath(imagetobesavedentity.getImagePath());
            imagetobesavedreturn.setImageDescription(imagetobesavedentity.getImageDescription());
            
            if(imagetobesaved.getSellerId()!=null)
            {
            	if(imagetobesaved.getPropertyId()!=null)
            	{
            	
            		propertyentity.getPropertyImageId().add(imagetobesavedentity);
            		propertyrepository.save(propertyentity);
            	}
            	else
            	{
            		userentity.getUserSellerId().setSellerCompanyLogoId(imagetobesavedentity);
            		 userrepository.save(userentity);
            	}
            		
            }
            else {
            	
                userentity.setUserImage(imagetobesavedentity);
                
                userrepository.save(userentity);
            }  
            
        } 
        catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("ImageHanler "+exception.getMessage());
			throw exception;
		}
		return imagetobesavedreturn;
	}
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	

	private void logg(String message) {
		// ***** Logging the message # # #
		log.error(message);
	}

	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public Image deleteImage(ImageHandlerModel imagetobedeleted, String saltstring)
			throws ServiceException, Exception {
		
        Image imagetobedeletedreturn=new Image();
        
        
        try
        {
        	
        	if(imagetobedeleted==null) {
        		throw new ServiceException(SERVICEEXCEPTION_NOIMAGEOBJ);
        	}
        	log.info("imagetobedeleted: "+imagetobedeleted);
        	
        	if(imagetobedeleted.getUserId()==null) {
        		throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
        	}
        	log.info("user Id: "+imagetobedeleted.getUserId());
        	if(saltstring==null)
        	{
        		throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
        	}
        	log.info("saltstring: "+saltstring);
        	if(imagetobedeleted.getImageId()==null)
        	{
        		throw new ServiceException(SERVICEEXCEPTION_NOIMAGEID);
        	}
        	log.info("Image Id to be delted: "+imagetobedeleted.getImageId());
        	
        	UsersEntity userentity =userrepository.findUserById(UUID.fromString(imagetobedeleted.getUserId()), saltstring);
        	
        	log.info("userEntity "+userentity);
        	
        	if(userentity==null) {
        		throw new ServiceException(SERVICEEXCEPTION_IMAGEUSER_NOTEXIST);
        	}
        	
        	ImageEntity imagetobedeletedentity=imagerepository.getImageByImageId(imagetobedeleted.getImageId());
        	
        	
        	if(imagetobedeletedentity==null)
        	{
        		throw new ServiceException(SERVICEEXCEPTION_NOIMAGEFOUND);
        	}
        	
        	log.info("ImageToBeDeleted "+imagetobedeletedentity);
        	
        	
//        	
//        	File filetobedeleted=new File(imagetobedeletedentity.getImagePath());
//           	if(!filetobedeleted.delete())
//            	{
//            		throw new ServiceException(SERVICEEXCEPTION_IMAGECANNOTBEDELETED);
//            	}
//            	
        	
        	
        	
        	if(imagetobedeleted.getSellerId()!=null)
        	{
        		if(userentity.getUserSellerId()!=null && imagetobedeleted.getSellerId().equals(userentity.getUserSellerId().getSellerId()) && userentity.isUserIsSeller() && userentity.getUserSellerId().isSellerIsActive())
        		{
        			if(imagetobedeleted.getPropertyId()!=null )
                    {
        				PropertyEntity propertyentity=null;
        				propertyentity=propertyrepository.findPropertyByPropertyId(imagetobedeleted.getPropertyId());
                    	if(propertyentity==null)
                    	{
                    		throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
                    	}
                    	
                    	
                    	propertyentity.getPropertyImageId().remove(imagetobedeletedentity);
                    	
                    	log.info("propertyentity "+propertyentity);
                    	
                    	propertyrepository.save(propertyentity);
                    	
                    }
        			else
        			{
        				if(!userentity.getUserSellerId().getSellerCompanyLogoId().getImageId().equals(imagetobedeletedentity.getImageId()))
        					throw new ServiceException(SERVICEEXCEPTION_IMAGEISNOTOWNED);
        					
        				userentity.getUserSellerId().setSellerCompanyLogoId(null);
        			}
        		}
        		else {
        			throw new ServiceException(SERVICEEXCEPTION_NOTANSELLER);
        		}
        	}
        	else {
        		if(!userentity.getUserImage().getImageId().equals(imagetobedeletedentity.getImageId()))
        			throw new ServiceException(SERVICEEXCEPTION_IMAGEISNOTOWNED);
        		userentity.setUserImage(null);
        	}
        	
        	imagetobedeletedentity.setImageIsActive(false);
        	imagetobedeletedentity.setImageDescription("deleted");
        	
        	log.info("ImageDeleted "+imagetobedeletedentity);
        	
        	imagerepository.save(imagetobedeletedentity);
        	
        	} 
        
        catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("ImageHanler "+exception.getMessage());
			throw exception;
		}
		return imagetobedeletedreturn;
	}
	
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public List<Image> savePropertyImages(ImageHandlerModel imagetobesaved,String saltstring) throws ServiceException,Exception
	{
		
		 	String uuid;
		 	String imagename;
	        String urltobesaved;
	        List<Image> imagestobesavedreturn=new ArrayList<>();
	        BufferedImage image = null;
	        
	      
	        try
	        {

	        	if(imagetobesaved.getUserId()==null) {
	        		throw new ServiceException(SERVICEEXCEPTION_NOUSERID);
	        	}
	        	log.info("user id: "+imagetobesaved.getUserId());
	        	//saltstring
	        	if(saltstring==null)
	        	{
	        		throw new ServiceException(SERVICEEXCEPTION_NOSALTSTRING);
	        	}
	        	log.info("saltstring: "+saltstring);
	        	uuid=imagetobesaved.getUserId();
	        	UsersEntity userentity =userrepository.findUserById(UUID.fromString(uuid), saltstring);
	        	
	        	log.info("userentity: "+userentity);
	        	
	        	if(userentity==null) {
	        		throw new ServiceException(SERVICEEXCEPTION_IMAGEUSER_NOTEXIST);
	        	}
	        	
	        	prop.load(inputStream); 
	        	urltobesaved=prop.getProperty(URL)+"user_"+uuid+"/";

	        	
	        	if(imagetobesaved.getSellerId()!=null)
	        	{	
		        		if((userentity.getUserSellerId()!=null) && userentity.isUserIsSeller() &&(userentity.getUserSellerId().getSellerId()).equals(imagetobesaved.getSellerId()))
		        		{
			        			urltobesaved=urltobesaved+"sellerid_"+imagetobesaved.getSellerId().toString()+"/";
			        			
			        			if(imagetobesaved.getPropertyId()!=null)
			                    {
			        				PropertyEntity propertyentity=null;
			                    	propertyentity=propertyrepository.getPropertyByPropertyId(imagetobesaved.getPropertyId());
			                    	if((propertyentity==null))
			                    		throw new ServiceException(SERVICEEXCEPTION_NOPROPERTY);
			                    	
			                    	log.info("propertyentity: "+propertyentity);

			                    	urltobesaved=urltobesaved+"property_"+imagetobesaved.getPropertyId().toString()+"/";
			                    	
			                    	List<ImageEntity> imagetobesavedentity=new ArrayList<>();
			                    	
			                    	String imagePath;
			                    	
			                    	
			                    	for(MultipartFile imageforproperty:imagetobesaved.getPropertyimages())
			                    	{
			                    		imagename=imageforproperty.getOriginalFilename();
			                    		imagePath=urltobesaved+imagename;
			                    		
			                    		log.info("imagePathforProperty: "+imagePath);
			                    		
			                    		ByteArrayInputStream fileinputstream=new ByteArrayInputStream(imageforproperty.getBytes());
			                    		image = ImageIO.read(fileinputstream);
			                    		File outputfile=new File(imagePath);
			                    		outputfile.mkdirs();
			                    		ImageIO.write(image, "jpg", outputfile);
			                    		
			                    		ImageEntity imageentity=new ImageEntity();
			                    		
			                    		imageentity.setImagePath(imagePath);
			                    		imageentity.setImageCreatedTime(LocalDateTime.now());
			                    		imageentity.setImageIsActive(true);
			            	            imageentity.setImageDescription(Base64Utils.encodeToString(imageforproperty.getBytes()));
			            	            
			            	            imagerepository.save(imageentity);
			            	            imagetobesavedentity.add(imageentity);
			            	            
			            	            propertyentity.getPropertyImageId().add(imageentity);
			            	            
			            	            Image imagemodel=new Image();
			            	            imagemodel.setImageCreatedTime(imageentity.getImageCreatedTime());
			            	            imagemodel.setImageDescription(imageentity.getImageDescription());
			            	            imagemodel.setImageId(imageentity.getImageId());
			            	            imagemodel.setImagePath(imageentity.getImagePath());
			            	            imagemodel.setImageIsActive(imageentity.isImageIsActive());
			            	            
			            	            imagestobesavedreturn.add(imagemodel);
			            	            
			                    	}
			                    	propertyrepository.save(propertyentity);   
		            	       }
		                }
	        	}
	        	else
	        	{
	        			
	        		throw new ServiceException(SERVICEEXCEPTION_NOTANSELLER);
	        			
	        	}
	        } 
	        catch(ServiceException exception) {
				logg(exception.getMessage());
				throw exception;
			//***** Catch for Exception # # #
			}catch(Exception exception) {
				logg("ImageHandler "+exception.getMessage());
				throw exception;
			}
			return imagestobesavedreturn;
	

}
}
