package com.project.terabit.service;



import java.util.List;
import java.util.UUID;

import com.project.terabit.model.Cart;
import com.project.terabit.model.SaveCart;


public interface CartService {
	
	public List<Cart> saveCart(String saltString,SaveCart cartToBeSaved) throws ServiceException,Exception;
	public List<Cart> deleteCart(String saltString,SaveCart userCartToBeDeleted) throws ServiceException,Exception;
	public List<Cart> getCart(String saltString,UUID userCart) throws ServiceException,Exception;

}
