package com.project.terabit.service;




import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.controller.ControllerException;
import com.project.terabit.entity.AdminEntity;
import com.project.terabit.entity.CartEntity;
import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.NotificationEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Admin;
import com.project.terabit.model.ImageHandlerModel;
import com.project.terabit.model.User;
import com.project.terabit.repository.AdminRepository;
import com.project.terabit.repository.CartRepository;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;
import com.project.terabit.validator.AdminValidator;

/**
 * The Class AdminServiceImpl.
 * CRUD operations of ADMIN is handled in this service
 */
@Service
@Transactional(readOnly=true)

public class AdminServiceImpl implements AdminService 
{
	
	/** The user repository. */
	//***** Autowiring the UserRepository # # #
	@Autowired
	UserRepository userRepository;
	
	/** The admin repository. */
	//***** Autowiring the AdminRepository # # #
	@Autowired
	AdminRepository adminRepository;
	
	/** The admin repository. */
	//***** Autowiring the AdminRepository # # #
	@Autowired
	SellerRepository sellerRepository;
	
	@Autowired
	PropertyRepository propertyRepository;

	@Autowired
	CartRepository cartRepository;
	
	/** The log. */
	//***** Declaring the logger for looging the information # # #
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	
	//****UTILITY ADMIN EXCEPTION ####
	//*****Defining exception Strings:
	private static final String SERVICEEXCEPTION_NOSALTSTRING ="ADMINSERVICE.SaltString_NULL";
	
	/** The Constant CONTROLLEREXCEPTION_NOUSER. */
	//***** Appropriate message to be thrown in each case craetion which is added in the application property controller Exception
	private static final String SERVICEEXCEPTION_NOUSER = "ADMINSERVICE.NO_USER_EXIST";
		
	/** The Constant CONTROLLEREXCEPTION_URLMISSMATCH. */
	private static final String SERVICEEXCEPTION_URLMISSMATCH = "ADMINSERVICE.saltString_mismatch";
	
	/** The Constant ADMIN_DO_NOT_EXISTS. */
	//***** Message related to Service Exception
	private static final String SERVICEEXCEPTION_ADMINNOTEXISTS = "DELETEADMINSERVICE.No_admin_exists";
	
	private static final String SERVICEEXCEPTION_ADMINIDNOTPASSED = "GETADMINSERVICE.Admin_id_not_passed";

	
	/** The Constant ADMIN_IS_NOT_ACTIVE. */
	private static final String SERVICEEXCEPTION_ADMINISNOTACTIVE = "DELETEADMINSERVICE.admin_is_not_active";
	
	/** The Constant SERVICEEXCEPTION_EXISTINGADMIN. */
	private static final String SERVICEEXCEPTION_EXISTINGADMIN = "CREATEADMINSERVICE.user_is_already_a_admin";
	
	/** The Constant SERVICEEXCEPTION_NOSELLER. */
	private static final String SERVICEEXCEPTION_NOSELLER = "CREATEADMINSERVICE.no_seller_for_admin";
	
	/** The Constant SERVICEEXCEPTION_NOUSERID. */
	private static final String SERVICEEXCEPTION_NOUSERID = "CREATEADMINSERVICE.no_userid_provided";

	
	/* (non-Javadoc)
	 * @see com.project.terabit.service.AdminService#createAdmin(java.lang.String, java.util.UUID)
	 */
	//***** OverRiding the createAdmin method of AdminService
	//****Creating admin for the user
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User createAdmin(String saltstring,UUID userId) throws Exception {
		try {
			//checking if user id is null
			if(userId==null) {
				throw new ControllerException(SERVICEEXCEPTION_NOUSERID);
			}
			log.info("user id: "+userId);
			//checking if saltstring is null
			if(saltstring==null) {
				throw new ControllerException(SERVICEEXCEPTION_NOSALTSTRING); 
			}
			log.info("saltstring: "+saltstring);
			//Initializing admin to be set to user
			Admin admin = new Admin();
			
			//Initializing user to be returned
			User user = new User();
			
			//Initializing adminEntity to save in db
			AdminEntity adminEntity = new AdminEntity();
			
			//Validating user id
			AdminValidator.validate(userId.toString());
			
			//Retrieving userentity from user table
			UsersEntity userEntity = userRepository.findActiveUsers(userId);
			
			//checking if no user present for the given user id
			if(userEntity==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			//*****Checking whether the obtained USER entity is null 
			if(!userEntity.getSaltString().equals(saltstring)) {
				throw new ControllerException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			
			//checking if the user is already a admin
			if(userEntity.getUserAdminId()!=null) {
				throw new ServiceException(SERVICEEXCEPTION_EXISTINGADMIN);
			}
			
			//checking if user is not a seller
			if(!userEntity.isUserIsSeller() && userEntity.getUserSellerId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_NOSELLER);
			}
			
			//populating admin to db
			adminEntity.setAdminCreatedTime(LocalDateTime.now());
			adminEntity.setAdminIsActive(false);
			adminEntity.setAdminModifiedTime(LocalDateTime.now());
			userEntity.setUserAdminId(adminEntity);
			adminRepository.save(adminEntity);
			userRepository.save(userEntity);
			admin.setAdminId(adminEntity.getAdminId());
			admin.setAdminSellerCount(adminEntity.getAdminSellerCount());
			user.setUserAdminId(admin);
			user.setUserId(userId);
			return user;
		//***** Catch for ServiceException # # #
		}catch(ServiceException exception) {
			logg(exception.getMessage());
			throw exception;
		//***** Catch for Exception # # #
		}catch(Exception exception) {
			logg("CreateAdmin "+exception.getMessage());
			throw exception;
		}
	}

	/* (non-Javadoc)
	 * @see com.project.terabit.service.AdminService#deleteAdmin(com.project.terabit.model.User, java.lang.String)
	 */
	//***** OverRiding the deleteadmin method of AdminService
	//****deleting existing admin 
	@Override
	@Transactional(readOnly = false,propagation=Propagation.REQUIRES_NEW)
	public User deleteAdmin(User userToBeDeleted,String saltString) throws ServiceException,Exception
	{	
		
		try {
			//checking if saltstring is passed
			if(saltString==null) 
			{
				throw new ControllerException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			log.info("saltstring: "+saltString);
			if(userToBeDeleted.getUserId()==null) {
				throw new ControllerException(SERVICEEXCEPTION_NOUSERID);
			}
			//*****Obtaining UserEntiy Object from User repository # # #
			UsersEntity userToBeDeletedEntity=userRepository.findActiveUsers(userToBeDeleted.getUserId());
			log.info("user id: "+userToBeDeleted.getUserId());
			
			//***** Checking if the user Exist or Not if yes throe an exxception to the user # # #
			if(userToBeDeletedEntity==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			
			//*****Checking whether the obtained USER entity is null 
			if(!userToBeDeletedEntity.getSaltString().equals(saltString)) {
				throw new ControllerException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			
			//***** Checking whether the obtained user has a Admin Object # # #
			if(userToBeDeletedEntity.getUserAdminId()==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_ADMINNOTEXISTS);
			}
			log.info("admin id: "+userToBeDeletedEntity.getUserAdminId());
			//*****Validating the saltString # # #
			//*****Checking whether the user is an active not a admin(If so thowring an Exception) # # #
			if(!userToBeDeletedEntity.getUserAdminId().isAdminIsActive())
				throw new ServiceException(SERVICEEXCEPTION_ADMINISNOTACTIVE);
			
			//*****inactivation of admin
			
			List<SellerEntity> sellerToBeDeleted = sellerRepository.getSellerByAdmin(userToBeDeleted.getUserAdminId().getAdminId());
			if(sellerToBeDeleted!=null && !sellerToBeDeleted.isEmpty()) {
				for(SellerEntity sellerEntity: sellerToBeDeleted) {
					sellerEntity.setSellerIsActive(false);
					UsersEntity userEntityForSeller = userRepository.findUserFromSellerId(sellerEntity.getSellerId());
					List<PropertyEntity> propertyToBeDeleted = sellerEntity.getSellerPropertyId();
					if(propertyToBeDeleted!=null && !propertyToBeDeleted.isEmpty()) {
						for(PropertyEntity propertyEntity: propertyToBeDeleted) {
							ImageHandlerServiceImpl imagehandlerservice = new ImageHandlerServiceImpl();
							if(propertyEntity.getPropertyImageId()!=null && !propertyEntity.getPropertyImageId().isEmpty()) {
								ImageHandlerModel imageHandler = new ImageHandlerModel();
								imageHandler.setUserId(userEntityForSeller.getUserId().toString());
								imageHandler.setPropertyId(propertyEntity.getPropertyId());
								imageHandler.setSellerId(sellerEntity.getSellerId());
								for (ImageEntity imageEntity : propertyEntity.getPropertyImageId()) {
									imageHandler.setImageId(imageEntity.getImageId());
									imagehandlerservice.deleteImage(imageHandler, userEntityForSeller.getSaltString());
								}
							}
							List<CartEntity> cartEntityList = cartRepository.findCartByPropertyId(propertyEntity.getPropertyId());
							if(cartEntityList!=null && !cartEntityList.isEmpty()) {
								for(CartEntity cartEntity: cartEntityList) {
									cartEntity.setCartIsActive(false);
									
								}
								cartRepository.saveAll(cartEntityList);
								
							}
							propertyEntity.setPropertyIsActive(false);
						}
					}
					sellerEntity.setSellerPropertyId(propertyToBeDeleted);
					if(sellerEntity.getSellerNotificationId()!=null && !sellerEntity.getSellerNotificationId().isEmpty()) {
						for(NotificationEntity notificationEntity:sellerEntity.getSellerNotificationId()) {
							notificationEntity.setNotificationIsActive(false);
						}
					}
				}
				
				userToBeDeletedEntity.getUserAdminId().setAdminSellerIds(sellerToBeDeleted);
			}
			
			if(userToBeDeletedEntity.getUserAdminId().getAdminNotificationIds()!=null && !userToBeDeletedEntity.getUserAdminId().getAdminNotificationIds().isEmpty()) {
				for(NotificationEntity notificationEntity: userToBeDeletedEntity.getUserAdminId().getAdminNotificationIds()) {
					notificationEntity.setNotificationIsActive(false);
				}
			}
			
			userToBeDeletedEntity.getUserAdminId().setAdminIsActive(false);

			
			//***** Saving things into the user Repository # # #
			userRepository.save(userToBeDeletedEntity);
			
			//initializing the user to be returned
			User userToBeReturned = new User();
			userToBeReturned.setUserId(userToBeDeletedEntity.getUserId());
			
			//returning the user
			return userToBeReturned;

			 
		}
		
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
			
			//***** Throwing the ServiceException
			throw serviceException;
		}
		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
			
			//***** Throwing the Exception
			throw exception;
		}
		
	}
	
	@Override
	public User getAdmin(Admin admin,String saltString) throws ServiceException,Exception
	{	
		
		try {
			//checking if saltstring is passed
			if(saltString==null) 
			{
				throw new ControllerException(SERVICEEXCEPTION_NOSALTSTRING);
			}
			log.info("saltstring: "+saltString);
			if(admin.getAdminId()==null) {
				throw new ServiceException(SERVICEEXCEPTION_ADMINIDNOTPASSED);
			}
			log.info("admin id: "+admin.getAdminId());
			//*****Obtaining UserEntiy Object from User repository # # #
			UsersEntity userEntity=userRepository.findUserFromAdminId(admin.getAdminId());
			
			//***** Checking if the user Exist or Not if yes throw an exception to the user # # #
			if(userEntity==null)
			{
				throw new ServiceException(SERVICEEXCEPTION_NOUSER);
			}
			
			//*****Checking whether the obtained USER entity is null 
			if(!userEntity.getSaltString().equals(saltString)) {
				throw new ControllerException(SERVICEEXCEPTION_URLMISSMATCH);
			}
			log.info("user id for admin is: "+userEntity.getUserId());
			
			//*****Validating the saltString # # #
			//*****Checking whether the user is an active not a admin(If so thowring an Exception) # # #
			if(!userEntity.getUserAdminId().isAdminIsActive())
				throw new ServiceException(SERVICEEXCEPTION_ADMINISNOTACTIVE);
			
			User user = new User();
			LoginUserServiceImpl loginservice = new LoginUserServiceImpl();
			user.setUserId(userEntity.getUserId());
			user.setUserContactNo(userEntity.getUserContactNo());
			user.setUserEmailId(userEntity.getUserEmailId());
			user.setUserIsActive(userEntity.isUserIsActive());
			user.setUserIsSeller(userEntity.isUserIsSeller());
			user.setUserFirstName(userEntity.getUserFirstName());
			user.setUserLastName(userEntity.getUserLastName());
			if(userEntity.getUserSellerId()!=null)
				user.setUserSellerId(loginservice.sellerForUser(userEntity));
			if(userEntity.getUserAdminId()!=null)
				user.setUserAdminId(loginservice.adminForUser(userEntity));
			if(userEntity.getUserCarts()!=null)
				user.setUserCarts(loginservice.cartForUser(userEntity.getUserCarts()));
			if(userEntity.getUserFeedbacks()!=null)
				user.setUserFeedbacks(loginservice.feedback(userEntity.getUserFeedbacks()));
			if(userEntity.getUserImage()!=null)
				user.setUserImage(loginservice.imageForUser(userEntity.getUserImage()));
			if(userEntity.getUserNotificationIds()!=null)
				user.setUserNotificationIds(loginservice.notificationlist(userEntity.getUserNotificationIds()));
			
			
			
		
			
			//returning the user
			return user;

			 
		}
		
		//***** Catch for ServiceException # # #
		catch(ServiceException serviceException)
		{
			//***** Logging the ServiceException
			logg(serviceException.getMessage());
			
			//***** Throwing the ServiceException
			throw serviceException;
		}
		
		//***** Catch for Exception # # #
		catch(Exception exception)
		{
			//***** Logging the Exception
			logg(exception.getMessage());
			
			//***** Throwing the Exception
			throw exception;
		}
		
	}
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		// ***** Logging the message # # #
		log.error(message);
	}



}
