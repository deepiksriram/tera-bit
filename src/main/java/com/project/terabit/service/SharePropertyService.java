package com.project.terabit.service;

import com.project.terabit.model.Property;
import com.project.terabit.model.ShareProperty;

public interface SharePropertyService {

	public Property shareProperty(ShareProperty shareProperty, String saltstring) throws Exception;
}
