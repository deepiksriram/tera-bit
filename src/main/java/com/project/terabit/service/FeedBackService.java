package com.project.terabit.service;

import java.util.List;

import com.project.terabit.model.Feedback;

public interface FeedBackService {
	
	public Feedback createFeedback(Feedback feedback,String saltString) throws Exception;
	
	public Feedback updateFeedback(Feedback feedback,String saltString) throws Exception;
	
	public List<Feedback> retriveFeedback(Feedback feedback,String saltString) throws Exception;


}
