package com.project.terabit.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.terabit.entity.FeedbackEntity;
import com.project.terabit.entity.ImageEntity;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.ViewedPropertyEntity;
import com.project.terabit.model.Feedback;
import com.project.terabit.model.Image;
import com.project.terabit.model.Property;
import com.project.terabit.model.ViewedProperty;
import com.project.terabit.repository.PropertyRepository;

// TODO: Auto-generated Javadoc
/**
 * The Class HomePagePropertyDisplayServiceImpl.
 */
@Service
@Transactional(readOnly=true)
public class HomePagePropertyDisplayServiceImpl implements HomePagePropertyDisplayService {
	
	/** The log. */
	Logger log=LoggerFactory.getLogger(this.getClass());
	
	/** The property repository. */
	@Autowired
	PropertyRepository propertyRepository;

	/** The Constant SERVICEEXCEPTION_NORESPONSEFROMDB. */
	public static final String SERVICEEXCEPTION_NORESPONSEFROMDB="SERVICEEXCEPTION.Count_returned_null";
	
	/** The Constant SERVICEEXCEPTION_EMPTYLIST. */
	public static final String SERVICEEXCEPTION_EMPTYLIST="SERVICEEXCEPTION.Empty_list_returned";
	
	/** The Constant SERVICEEXCEPTION_INVALIDLASTPROPERTYID. */
	public static final String SERVICEEXCEPTION_INVALIDLASTPROPERTYID="SERVICEEXCEPTION.last_property_id_is_negative";
	
	//property diplay for home page
	
	/* (non-Javadoc)
	 * @see com.project.terabit.service.HomePagePropertyDisplayService#propertyDisplay(java.math.BigInteger)
	 */
	//input propertyid
	//output list of property
	public List<Property> propertyDisplay(BigInteger lastpropertyid) throws Exception {
		try {
			List<PropertyEntity> propertyEntityList=new ArrayList<>();
			List<Property> propertyList;
			if(lastpropertyid.intValue()<0) {
				throw new ServiceException(SERVICEEXCEPTION_INVALIDLASTPROPERTYID);
			}
			BigInteger totalProperty=propertyRepository.getTotalPropertyCount();
			if(totalProperty==null) {
				throw new ServiceException(SERVICEEXCEPTION_NORESPONSEFROMDB);
			}
			if(totalProperty.compareTo(lastpropertyid)>=0) {
				propertyEntityList=propertyRepository.getpropertyForHomePage(lastpropertyid);
			}
			if(totalProperty.compareTo(lastpropertyid)<0){
				propertyEntityList=propertyRepository.getpropertyForHomePageLastfew(lastpropertyid);
			}
			if(propertyEntityList.isEmpty()) {
				throw new ServiceException(SERVICEEXCEPTION_EMPTYLIST);
			}
			else {
				propertyList=this.propertylist(propertyEntityList);
			}
		return propertyList;
		}catch(ServiceException exception) {
			
			logg(exception.getMessage());
			throw exception;
		}catch(Exception exception) {
			logg("Login "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Feedback.
	 *
	 * @param feedbackentitylist the feedbackentitylist
	 * @return the list of feedback list
	 */
	public List<Feedback> feedback(List<FeedbackEntity> feedbackentitylist){
		try {
		List<Feedback> feedbacklist=new ArrayList<>();
		for (FeedbackEntity feedback1 : feedbackentitylist) {
			Feedback feedbackToAddToProperty= new Feedback();
			feedbackToAddToProperty.setFeedbackCreatedBy(feedback1.getFeedbackCreatedBy());
			feedbackToAddToProperty.setFeedbackDescription(feedback1.getFeedbackDescription());
			feedbackToAddToProperty.setFeedbackGivenBy(feedback1.getFeedbackGivenBy());
			feedbackToAddToProperty.setFeedbackId(feedback1.getFeedbackId());
			feedbackToAddToProperty.setFeedbackRating(feedback1.getFeedbackRating());
			feedbackToAddToProperty.setFeedbackModifiedTime(feedback1.getFeedbackModifiedTime());
			feedbacklist.add(feedbackToAddToProperty);
		}	return feedbacklist;
		}catch(Exception exception) {
			logg("feedback "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Image.
	 *
	 * @param userImageEntity the user image entity
	 * @return the list of image
	 */
	public List<Image> image(List<ImageEntity> userImageEntity) {
		try {
			List<Image> imagelist=new ArrayList<>();
			for (ImageEntity image : userImageEntity) {
			Image image1= new Image();
			image1.setImageCreatedTime(image.getImageCreatedTime());
			image1.setImageDescription(image.getImageDescription());
			image1.setImageId(image.getImageId());
			image1.setImageIsActive(image.isImageIsActive());
			image1.setImagePath(image.getImagePath());
			imagelist.add(image1);
		}
			return imagelist;
		}catch(Exception exception) {
			logg("imageForUser "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Viewed property.
	 *
	 * @param viewedPropertyEntityList the viewed property entity list
	 * @return the list
	 */
	public List<ViewedProperty> viewedProperty (List<ViewedPropertyEntity> viewedPropertyEntityList){
		try {
		List<ViewedProperty> viewedPropertyOfProperty=new ArrayList<>();
		for (ViewedPropertyEntity viewedproperty : viewedPropertyEntityList) {
			ViewedProperty viewproperty=new ViewedProperty();
			viewproperty.setViewedPropertyId(viewedproperty.getViewedPropertyId());
			viewproperty.setViewedPropertyPropertyId(this.property(viewedproperty.getViewedPropertyPropertyId()));
			viewproperty.setViewedSellerId(viewedproperty.getViewedSellerId());
			viewproperty.setViewedTime(viewedproperty.getViewedTime());
			viewproperty.setViewedUserId(viewedproperty.getViewedUserId());
			viewedPropertyOfProperty.add(viewproperty);
		}
		return viewedPropertyOfProperty;
		}catch(Exception exception) {
			logg("viewedproperty "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Propertylist.
	 *
	 * @param propertyEntityList the property entity list
	 * @return the list
	 */
	public List<Property> propertylist(List<PropertyEntity> propertyEntityList){
		try {
		List<Property> propertyList=new ArrayList<>();
		for (PropertyEntity property : propertyEntityList) {
			Property p=new Property();
			p.setPropertyCent(property.getPropertyCent());
			p.setPropertyCity(property.getPropertyCity());
			p.setPropertyCountry(property.getPropertyCountry());
			p.setPropertyCreatedTime(property.getPropertyCreatedTime());
			p.setPropertyDescription(property.getPropertyDescription());
			p.setPropertyEsteematedAmount(property.getPropertyEsteematedAmount());
			p.setPropertyId(property.getPropertyId());
			p.setPropertyIsActive(property.isPropertyIsActive());
			p.setPropertyLandmark(property.getPropertyLandmark());
			p.setPropertyLatitude(property.getPropertyLongitude());
			p.setPropertyLongitude(property.getPropertyLongitude());
			p.setPropertyModifiedTime(property.getPropertyModifiedTime());
			p.setPropertyOwnedBy(property.getPropertyOwnedBy());
			p.setPropertyState(property.getPropertyState());
			p.setPropertyType(property.getPropertyType());
			p.setPropertyViewedCount(property.getPropertyViewedCount());
			if(!property.getPropertyFeedbackId().isEmpty()) {
				p.setPropertyFeedbackIds(this.feedback(property.getPropertyFeedbackId()));
			}
			if(!property.getPropertyImageId().isEmpty()) {
				p.setPropertyImageIds(this.image(property.getPropertyImageId()));
			}
			if(!property.getPropertyViewedId().isEmpty()) {
				p.setPropertyViewedIds(this.viewedProperty(property.getPropertyViewedId()));
			}
			propertyList.add(p);
		}
		return propertyList;
		}catch(Exception exception) {
			logg("property "+exception.getMessage());
			throw exception;
		}
	}
	
	
	/**
	 * Property.
	 *
	 * @param propertyEntity the property entity
	 * @return the property
	 */
	public Property property(PropertyEntity propertyEntity){
		try {
			
			Property property=new Property();
			property.setPropertyCent(propertyEntity.getPropertyCent());
			property.setPropertyCity(propertyEntity.getPropertyCity());
			property.setPropertyCountry(propertyEntity.getPropertyCountry());
			property.setPropertyCreatedTime(propertyEntity.getPropertyCreatedTime());
			property.setPropertyDescription(propertyEntity.getPropertyDescription());
			property.setPropertyEsteematedAmount(propertyEntity.getPropertyEsteematedAmount());
			property.setPropertyId(propertyEntity.getPropertyId());
			property.setPropertyIsActive(propertyEntity.isPropertyIsActive());
			property.setPropertyLandmark(propertyEntity.getPropertyLandmark());
			property.setPropertyLatitude(propertyEntity.getPropertyLongitude());
			property.setPropertyLongitude(propertyEntity.getPropertyLongitude());
			property.setPropertyModifiedTime(propertyEntity.getPropertyModifiedTime());
			property.setPropertyOwnedBy(propertyEntity.getPropertyOwnedBy());
			property.setPropertyState(propertyEntity.getPropertyState());
			property.setPropertyType(propertyEntity.getPropertyType());
			property.setPropertyViewedCount(propertyEntity.getPropertyViewedCount());
			if(!propertyEntity.getPropertyFeedbackId().isEmpty()) {
				property.setPropertyFeedbackIds(this.feedback(propertyEntity.getPropertyFeedbackId()));
			}
			if(!propertyEntity.getPropertyImageId().isEmpty()) {
				property.setPropertyImageIds(this.image(propertyEntity.getPropertyImageId()));
			}
			if(!propertyEntity.getPropertyViewedId().isEmpty()) {
				property.setPropertyViewedIds(this.viewedProperty(propertyEntity.getPropertyViewedId()));
			}
		return property;
		}catch(Exception exception) {
			logg("property "+exception.getMessage());
			throw exception;
		}
	}
	
	/**
	 * Logg.
	 *
	 * @param message the message
	 */
	private void logg(String message) {
		log.error(message);
	}

}
