package com.project.terabit.service;

import org.springframework.stereotype.Component;

import com.project.terabit.model.Image;

@Component
public interface ImageService {

	public Image createImage(String saltstring,Image image) throws Exception;
	
	public Image deleteImage(String saltstring,Image image) throws Exception;
	
	public Image updateImage(String saltstring,Image image) throws Exception;
}
