package com.project.terabit.repository;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.ImageEntity;

@Repository
public interface ImageRepository extends CrudRepository<ImageEntity, BigInteger>{

	@Query(value="SELECT * FROM image i WHERE i.image_path = :imagePath",nativeQuery=true)
	public ImageEntity getImageByPath(@Param (value="imagePath") String imagePath);
	
	@Query(value="SELECT * FROM image i WHERE i.image_id = :imageId and i.image_is_active = 't' ",nativeQuery=true)
	public ImageEntity getImageByImageId(@Param (value="imageId") BigInteger imageId);
}
