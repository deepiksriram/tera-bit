package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.CartEntity;

@Repository
public interface CartRepository extends CrudRepository<CartEntity,BigInteger>{
	
	@Query(value="select * from cart  where cart_id=:cartId",nativeQuery=true)
	public CartEntity findCartByCartId(@Param(value="cartId") BigInteger cartId);

	@Query(value="select * from cart  where cart_property=:propertyId",nativeQuery=true)
	public List<CartEntity> findCartByPropertyId(@Param(value="propertyId") BigInteger propertyId);
}
