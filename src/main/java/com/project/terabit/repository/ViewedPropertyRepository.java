package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.UsersEntity;
import com.project.terabit.entity.ViewedPropertyEntity;

@Repository
public interface ViewedPropertyRepository extends CrudRepository<ViewedPropertyEntity, BigInteger>{

	@Query(value = "SELECT * FROM Viewed_property v WHERE v.viewed_property_property = :propertyId AND v.viewed_user_id = :userId", nativeQuery = true)
	public ViewedPropertyEntity findDistinctProperty(@Param(value = "propertyId") BigInteger propertyId,@Param(value = "userId") UUID userId);
	
	@Query(value = "SELECT * FROM users u  WHERE u.seller_id = :sellerId",nativeQuery=true)
	public UsersEntity findUserIdFromSellerId(@Param(value = "sellerId") BigInteger sellerId);
	
	@Query(value = "SELECT v.viewed_property_property FROM viewed_property v WHERE  v.viewed_user_id = :user_id", nativeQuery=true)
	public List<BigInteger> getPropertyIdByUserId(@Param(value = "user_id") UUID userId);
	
}

