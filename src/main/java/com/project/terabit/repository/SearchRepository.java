package com.project.terabit.repository;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.terabit.entity.PropertyEntity;

@Repository
public interface SearchRepository extends CrudRepository<PropertyEntity, BigInteger>{

	
	@Query(value="SELECT p.property_id FROM property p INNER JOIN seller_seller_property_ids se\r\n" + 
			"ON p.property_id = se.seller_property_ids_property_id INNER JOIN seller s \r\n" + 
			"ON se.seller_entity_seller_id = s.seller_id INNER JOIN users u ON s.seller_id = u.seller_id \r\n" + 
			"WHERE p.property_id > ?2 AND s.seller_is_active = 't' AND u.user_is_active = 't' AND p.property_is_active = 't' \r\n" + 
			"AND (u.user_first_name ilike %?1% OR u.user_last_name ilike %?1% OR s.seller_company_name ilike %?1% \r\n" + 
			"or p.property_city ilike %?1% OR p.property_landmark ilike %?1% OR p.property_state ilike %?1%) \r\n" + 
			"order by 1 asc limit 10" ,nativeQuery = true )
	public List<BigInteger> getPropertyIdBySearch(@Param (value="keyword") String searchKeyword,@Param (value="property_id") BigInteger propertyId);

	
	@Query(value="SELECT v.viewed_property_property FROM viewed_property v WHERE v.viewed_user_id = :userId",nativeQuery=true)
	public List<BigInteger> getPropertyIdByViewedProperty(@Param(value="userId") UUID userId);
}
