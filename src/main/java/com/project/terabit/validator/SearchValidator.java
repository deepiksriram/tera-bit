package com.project.terabit.validator;

public class SearchValidator {

	private SearchValidator() {
	    throw new IllegalStateException("Utility class");
	  }
	
	private static final String SEARCHKEYWORDEXCEPTION = "SEARCH.invalid_keyword";
	
	public static void validate(String string) throws Exception {
		if(string!=null && (!validateSearchKeyword(string))){
			throw new Exception(SEARCHKEYWORDEXCEPTION);
		}
	}
	public static Boolean validateSearchKeyword(String keyword) {
		Boolean flag = false;
		if(keyword.matches("[A-Za-z][A-Za-z][A-Za-z ]+")) {
			flag = true;
		}return flag;
	}
}
