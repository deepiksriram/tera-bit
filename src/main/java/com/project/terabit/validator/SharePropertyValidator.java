package com.project.terabit.validator;

import com.project.terabit.model.ShareProperty;

public class SharePropertyValidator {

	
	private SharePropertyValidator() {
	    throw new IllegalStateException("Utility class");
	  }
	
	/** The Constant mailIdException. */
	private static final String MAILIDEEXCEPTION = "SHAREPROPERTYVALIDATOR.invalid_email_id";
	
	private static final String USERIDEXCEPTION = "SHAREPROPERTYVALIDATOR.invalid_user_id";
	
	public static void validate(ShareProperty shareProperty) throws Exception {
		if(!validateUserId(shareProperty.getUserId().toString())) throw new Exception(USERIDEXCEPTION);
		if(!validatemailId(shareProperty.getReceiverEmailId()))  throw new Exception(MAILIDEEXCEPTION);
		
	}
	public static Boolean validateUserId(String userId) {
		Boolean flag = false;
		if(userId.matches("[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}")) {
			flag = true;
		}return flag;
	}
	
	public static Boolean validatemailId(String mailId) {
		boolean flag=false;
		if(mailId.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$" )) {
			flag=true;
		}
		return flag;
		
		
	}
}
