package com.project.terabit.validator;

import java.math.BigInteger;

import com.project.terabit.model.User;


/**@author joshua shakespeare
/**
 * The Class UpdateValidator.
 */
public class UpdateUserValidator {
	
	/**
	 * Instantiates a new update user validator.
	 */
	private UpdateUserValidator() {
	    throw new IllegalStateException("Utility class");
	  }
	
	/** The Constant MAILIDEEXCEPTION. */
	private static final String MAILIDEEXCEPTION = "UPDATEUSERVALIDATOR.invalid_email_id";
	
	/** The Constant PHONENUMBEREXCEPTION. */
	private static final String PHONENUMBEREXCEPTION = "UPDATEUSERVALIDATOR.invalid_phone_no";
	
	private static final String FIRSTNAMEEXCEPTION = "UPDATEUSERVALIDATOR.invalid_firstname";
	private static final String LASTNAMEEXCEPTION = "UPDATEUSERVALIDATOR.invalid_lastname";
	
	/**
	 * Update info validate.
	 *
	 * @param user the user
	 * @throws Exception the exception
	 */
	public static void updateInfoValidate(User user) throws Exception {
		if(!validatemailId(user.getUserEmailId()))  throw new Exception(MAILIDEEXCEPTION);
		if(!validatePhoneNumber(user.getUserContactNo()))  throw new Exception(PHONENUMBEREXCEPTION);
		if(!validateFirstName(user.getUserFirstName())) throw new Exception(FIRSTNAMEEXCEPTION);
		if(!validateLastName(user.getUserLastName())) throw new Exception(LASTNAMEEXCEPTION);
	}
	
	/**
	 * Validatemail id.
	 *
	 * @param mailId the mail id
	 * @return the boolean
	 */
	public static Boolean validatemailId(String mailId) {
		boolean flag=false;
		if(mailId.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$" )) {
			flag=true;
		}return flag;
		
		
	}
	
	/**
	 * Validate phone number.
	 *
	 * @param phoneNo the phone no
	 * @return the boolean
	 */
	public static Boolean validatePhoneNumber(BigInteger phoneNo) {
		boolean flag=false;
		if(phoneNo.toString().matches("[6-9][0-9]{9}") && (!phoneNo.toString().matches("[7]{10}")) 
				&& (!phoneNo.toString().matches("[6]{10}"))
				&& (!phoneNo.toString().matches("[8]{10}")) && (!phoneNo.toString().matches("[9]{10}"))) {
			flag=true;
		}return flag;
	}
	
	public static Boolean validateFirstName(String firstName) {
		boolean flag = false;
		if(firstName.matches("[A-Za-z][a-z]*")) {
			flag = true;
		}return flag;
	}
	public static Boolean validateLastName(String lastName) {
		boolean flag = false;
		if(lastName.matches("[A-Za-z]+")) {
			flag = true;
		}return flag;
	}

}
