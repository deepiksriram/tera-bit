package com.project.terabit.service;

import java.math.BigInteger;
import java.util.List;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.FeedbackEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.Feedback;
import com.project.terabit.repository.FeedBackRepository;
import com.project.terabit.repository.UserRepository;

public class FeedBackServiceTest extends TerabitApplicationTests{
	
	
	@Mock 
	UserRepository userRepository;
	
	@Mock
	FeedBackRepository feedbackRepository;
	
	@InjectMocks
	FeedBackServiceImpl feedBackServiceImpl;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	
	String saltString ="aaaaaaaa";
	
	public Feedback getFeedBack()
	{
		Feedback feedBack=new Feedback();
		feedBack.setFeedbackCreatedBy("solai");
		feedBack.setFeedbackDescription("Good");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		feedBack.setFeedbackGivenBy(UUID.fromString(uuid));
		feedBack.setFeedbackRating(5);
		feedBack.setMessage("hi");
		return feedBack;
		
	}
	
	public UsersEntity getUserEntity() throws Exception{
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		UsersEntity userEntity=new UsersEntity();
		userEntity.setUserId(UUID.fromString(uuid));
		userEntity.setUserFirstName("solai");
		
		return userEntity;
		
	}
	
	public FeedbackEntity getFeedBackEntity()
	{
		FeedbackEntity feedbackEntity=new FeedbackEntity();
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		feedbackEntity.setFeedbackCreatedBy("solai");
		feedbackEntity.setFeedbackDescription("Good");
		feedbackEntity.setFeedbackGivenBy(UUID.fromString(uuid));
		feedbackEntity.setFeedbackId(BigInteger.valueOf(1));
		feedbackEntity.setFeedbackRating(5);
		
		return feedbackEntity;
	}
	
	@Test
	public void isSaltStringNull() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.no_saltstring");
		
		Feedback feedBack=this.getFeedBack();
		
		saltString =null;
		
		feedBackServiceImpl.createFeedback(feedBack, saltString);
		
	}
	
	@Test
	public void isFeedBackByIsNull() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.invalid_user_id");
		
		Feedback feedBack=this.getFeedBack();
		feedBack.setFeedbackGivenBy(null);
		
		feedBackServiceImpl.createFeedback(feedBack, saltString);
		
	}
	
	@Test
	public void isUserNotExist() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.No_User_Exists");
		
		Feedback feedBack=this.getFeedBack();
		
		FeedbackEntity feedBackEntity=this.getFeedBackEntity();
		
		feedBackEntity.setFeedbackGivenBy(null);
		
		Mockito.when(userRepository.findUserByUserId(feedBack.getFeedbackGivenBy())).thenReturn(null);
		
		feedBackServiceImpl.createFeedback(feedBack, saltString);
		
	}
	
	@Test
	public void isSaltStringNullUpdate() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.no_saltstring");
		
		Feedback feedBack=this.getFeedBack();
		
		saltString =null;
		
		feedBackServiceImpl.updateFeedback(feedBack, saltString);
		
	}
	
	@Test
	public void isFeedBackByIsNullUpdate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.invalid_user_id");
		
		Feedback feedBack=this.getFeedBack();
		feedBack.setFeedbackGivenBy(null);
		
		feedBackServiceImpl.updateFeedback(feedBack, saltString);
		
	}
	
	@Test
	public void isUserNotExistUpdate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.No_User_Exists");
		
		Feedback feedBack=this.getFeedBack();
		
		FeedbackEntity feedBackEntity=this.getFeedBackEntity();
		
		feedBackEntity.setFeedbackGivenBy(null);
		
		Mockito.when(userRepository.findUserByUserId(feedBack.getFeedbackGivenBy())).thenReturn(null);
		
		feedBackServiceImpl.updateFeedback(feedBack, saltString);
		
	}
	
	@Test
	public void isFeedBackIsNull() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.invalid_feedback_id");
		
		Feedback feedBack=this.getFeedBack();
		feedBack.setFeedbackId(BigInteger.valueOf(1l));
		
		FeedbackEntity feedBackEntity=this.getFeedBackEntity();
		
		feedBackEntity.setFeedbackGivenBy(null);
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserById(userEntity.getUserId(), saltString)).thenReturn(userEntity);
		
		Mockito.when(feedbackRepository.findFeedbacksByFeedbackId(feedBack.getFeedbackId())).thenReturn(null);
		
		feedBackServiceImpl.updateFeedback(feedBack, saltString);
	}
	
	@Test
	public void isSaltStringNullRetrive() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.no_saltstring");
		
		Feedback feedBack=this.getFeedBack();
		
		saltString =null;
		
		feedBackServiceImpl.retriveFeedback(feedBack, saltString);
		
	}
	
	@Test
	public void isFeedBackByIsNullRetrive() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.invalid_user_id");
		
		Feedback feedBack=this.getFeedBack();
		feedBack.setFeedbackGivenBy(null);
		
		feedBackServiceImpl.retriveFeedback(feedBack, saltString);
		
	}
	
	@Test
	public void isUserNotExistRetrive() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.No_User_Exists");
		
		Feedback feedBack=this.getFeedBack();
		
		FeedbackEntity feedBackEntity=this.getFeedBackEntity();
		
		feedBackEntity.setFeedbackGivenBy(null);
		
		Mockito.when(userRepository.findUserByUserId(feedBack.getFeedbackGivenBy())).thenReturn(null);
		
		feedBackServiceImpl.retriveFeedback(feedBack, saltString);
		
	}
	
	@Test
	public void isFeedBackIsNullRetrive() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("DELETEFEEDBACKSERVICE.No_feedback");
		
		Feedback feedBack=this.getFeedBack();
		
		
		List<FeedbackEntity> feedbackEntityList = null;
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserById(userEntity.getUserId(), saltString)).thenReturn(userEntity);
		
		Mockito.when(feedbackRepository.findFeedbacksByUserId(userEntity.getUserId())).thenReturn(feedbackEntityList);
		
		feedBackServiceImpl.retriveFeedback(feedBack, saltString);
	}
}
