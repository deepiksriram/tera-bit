package com.project.terabit.service;

import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.model.User;
import com.project.terabit.repository.UserRepository;

 /* The Class DeleteUserServiceTest.
 */
public class DeleteUserServiceTest extends TerabitApplicationTests{

	/** The user repository. */
	@Mock 
	UserRepository userRepository;
	
	/** The service. */
	@InjectMocks
	SignUpUserServiceImpl service;

	/** The expected exception. */
	@Rule
	public ExpectedException expectedException = ExpectedException.none();

	/**
	 * Delete user test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void deleteUserTest() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("UTILITYSERVICE.invalid_user");
		String uuid = "8705b317-3bda-4ef7-abf7-2d951edc4ee7";
		User user = new User();
		user.setUserId(UUID.fromString(uuid));
		user.setUserIsActive(true);
		String saltstring = "aaaaaa";
		Mockito.when(userRepository.findUserByUserId(UUID.fromString(uuid))).thenReturn(null);
		service.deleteUser(saltstring,user);	
	}

}
////
