package com.project.terabit.service;

import java.math.BigInteger;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.User;
import com.project.terabit.repository.UserRepository;

public class LoginUserServiceTest extends TerabitApplicationTests {
	
	@Mock
	UserRepository userrepository;
	
	@InjectMocks	
	LoginUserServiceImpl loginservice;
	
	 @Rule
	 public ExpectedException expectedException = ExpectedException.none();
	 
	 public User getUser() {
		 User user = new User();
	     user.setUserEmailId("lisadeepik@gmail.com");
	     user.setUserContactNo(BigInteger.valueOf(8681999437l));
	     user.setUserPassword("Deepik@lisa79");
	     return user;
	 }
	 public UsersEntity getUserEntity() {
		 UsersEntity userentity=new UsersEntity();
         userentity.setUserEmailId("lisadeepik@gmail.com");
         userentity.setUserPassword("-1165077272");
         userentity.setUserIsActive(true);
         return userentity;
	 }
	 
	 @Test
	 public void isUserDetailsEnteredMail() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("Service.Input_Data_Missing");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserEmailId(null);
	     user_to_be_logged_in.setUserPassword(null);
	     
	     loginservice.login(user_to_be_logged_in);
	     
	 }
	 @Test
	 public void isUserDetailsEnteredContactNumber() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("Service.Input_Data_Missing");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserContactNo(null);
	     user_to_be_logged_in.setUserPassword(null);
	     
	     loginservice.login(user_to_be_logged_in);
	     
	 }
	 
	 @Test
	 public void isUserNotExist() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SERVICE.No_User_Exists");
	     
	     User user_to_be_logged_in=this.getUser();
	     
	     Mockito.when(userrepository.findUserByEmailID("lisadeepik@gmail.com")).thenReturn(null);
	     loginservice.login(user_to_be_logged_in);   
	 }
	 
	 @Test
	 public void IsPassWordMismatch() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SERVICE.Username_Password_MissMatch");
	     
	     User user_to_be_logged_in=this.getUser();
	     
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     user_to_be_logged_in_entity.setUserVerifed(true);
	     user_to_be_logged_in_entity.setUserPassword(user_to_be_logged_in.getUserPassword()+"a");
	     Mockito.when(userrepository.findUserByEmailID("lisadeepik@gmail.com")).thenReturn(user_to_be_logged_in_entity);
	     loginservice.login(user_to_be_logged_in); 
	 }
	 
	 @Test
	 public void userNotActiveException() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("SERVICE.User_Not_Active");
	     
	     User user_to_be_logged_in=this.getUser();
	     
	     UsersEntity user_to_be_logged_in_entity=this.getUserEntity();
	     user_to_be_logged_in_entity.setUserIsActive(false);
	     user_to_be_logged_in_entity.setUserVerifed(true);
	     Mockito.when(userrepository.findUserByEmailID("lisadeepik@gmail.com")).thenReturn(user_to_be_logged_in_entity);
         loginservice.login(user_to_be_logged_in);   
	 }
	 
	
	 
	//***** Validator # # #
	 @Test
	 public void isValidEmailIdNormalString() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_email_id");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserEmailId("solai");
	     
	     loginservice.login(user_to_be_logged_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutSC() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_email_id");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserEmailId("solai.com");
	     
	     loginservice.login(user_to_be_logged_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutDot() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_email_id");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserEmailId("solai@gmailcom");
	     
	     loginservice.login(user_to_be_logged_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithExtraLengthAfterDot() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_email_id");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserEmailId("solai@gmail.comin");
	     
	     loginservice.login(user_to_be_logged_in);
	     
	 }
	 
	 @Test
	 public void isValidContactNumberLessLength() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_phone_no");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserContactNo(BigInteger.valueOf(868199943l));
	     
	     loginservice.login(user_to_be_logged_in);
		 
	 }
	 
	 @Test
	 public void isValidContactNumberMoreLength() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("VALIDATOR.invalid_phone_no");
	     
	     User user_to_be_logged_in=this.getUser();
	     user_to_be_logged_in.setUserContactNo(BigInteger.valueOf(868199943333l));
	     
	     loginservice.login(user_to_be_logged_in);
		 
	 }
//	 @Test
//	 public void isValidContactNumberStartWithOne() throws Exception{
//		 expectedException.expect(Exception.class);
//	     expectedException.expectMessage("VALIDATOR.invalid_phone_no");
//	     
//	     User user_to_be_logged_in=this.getUser();
//	     user_to_be_logged_in.setUserContactNo(BigInteger.valueOf(1681999437l));
//	     
//	     loginservice.login(user_to_be_logged_in);
//		 
//	 }
	
}
