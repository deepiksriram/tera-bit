package com.project.terabit.service;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.AdminEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.User;
import com.project.terabit.repository.AdminRepository;
import com.project.terabit.repository.UserRepository;

public class AdminServiceTest extends TerabitApplicationTests{
	@Mock
	UserRepository userRepository;
	
	@Mock
	AdminRepository adminRepository;
	
	@InjectMocks
	AdminServiceImpl adminService;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void isSaltStringNullCreate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("ADMINSERVICE.SaltString_NULL");
		
		UUID uuid = UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449");
		String saltString=null;
		
	
		
		adminService.createAdmin(saltString,uuid);
	}
	
	@Test
	public void isUserIdNull() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATEADMINSERVICE.no_userid_provided");
		
		UUID uuid = null;
		String saltString=null;
		
	
		
		adminService.createAdmin(saltString,uuid);
	
	}
	@Test
	public void isUserNotExistCreate() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SERVICE.NO_USER_EXIST");
		
		UUID uuid = UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449");
		String saltString="aaaaaaaaa";
		
		Mockito.when(userRepository.findUserByUserId(uuid)).thenReturn(null);
		
		adminService.createAdmin(saltString,uuid);
		
	}
	
	@Test
	public void isUserIsAdminCreate() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("CREATEADMINSERVICE.user_is_already_a_admin");
		
		UUID uuid = UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a449");
		String saltString="qw";
		
		AdminEntity adminToBeDeletedEntity=new AdminEntity();
		adminToBeDeletedEntity.setAdminId(BigInteger.valueOf(1));
		adminToBeDeletedEntity.setAdminIsActive(true);
		
		
		UsersEntity userAdminEntity=new UsersEntity();
		userAdminEntity.setUserId(uuid);
		userAdminEntity.setUserAdminId(adminToBeDeletedEntity);
		userAdminEntity.setSaltString(saltString);
		
		
		Mockito.when(userRepository.findActiveUsers(uuid)).thenReturn(userAdminEntity);
		
		adminService.createAdmin(saltString,uuid);
	}

	
	@Test
	public void isSaltStringNullDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("ADMINSERVICE.SaltString_NULL");
		
		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a449";
		String saltString=null;
		
		User userAdminToBeDeleted=new User();
		userAdminToBeDeleted.setUserId(UUID.fromString(uuid));
		
		adminService.deleteAdmin(userAdminToBeDeleted,saltString);
	}
	
	
	@Test
	public void isAdmin() throws Exception
	{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SERVICE.NO_USER_EXIST");
		
		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a449";
		String saltString="qw";
		
		User userAdminToBeDeleted=new User();
		userAdminToBeDeleted.setUserId(UUID.fromString(uuid));
		
		AdminEntity adminToBeDeletedEntity=new AdminEntity();
		adminToBeDeletedEntity.setAdminId(BigInteger.valueOf(1));
		adminToBeDeletedEntity.setAdminIsActive(false);
		
		
		UsersEntity userAdminEntity=new UsersEntity();
		userAdminEntity.setUserId(UUID.fromString(uuid));
		userAdminEntity.setUserAdminId(adminToBeDeletedEntity);
		
		//***** Using When and then and reurning $NULL$ while called # # # 
		
		Mockito.when(userRepository.findUserByUserId(userAdminToBeDeleted.getUserId())).thenReturn(null);
		
		//***** Calling delteAdmin in AdminSerive $to test the delteAdmin with Invalid UserId$ # # #
		adminService.deleteAdmin(userAdminToBeDeleted,saltString);
		}
	
	@Test
	public void isUrlMissMatchDelete() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SERVICE.saltString_mismatch");
		
		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a449";
		String saltString="qw";
		
		User userAdminToBeDeleted=new User();
		userAdminToBeDeleted.setUserId(UUID.fromString(uuid));
		
		AdminEntity adminToBeDeletedEntity=new AdminEntity();
		adminToBeDeletedEntity.setAdminId(BigInteger.valueOf(1));
		adminToBeDeletedEntity.setAdminIsActive(false);
		
		UsersEntity userAdminEntity=new UsersEntity();
		userAdminEntity.setUserId(UUID.fromString(uuid));
		userAdminEntity.setSaltString(saltString);
		userAdminEntity.setUserAdminId(adminToBeDeletedEntity);
		
		//***** Using When and then and reurning $NULL$ while called # # # 
		
		Mockito.when(userRepository.findActiveUsers(userAdminToBeDeleted.getUserId())).thenReturn(userAdminEntity);
		
		//***** Calling delteAdmin in AdminSerive $to test the delteAdmin with Invalid UserId$ # # #
		adminService.deleteAdmin(userAdminToBeDeleted,saltString+"a");
		
	}
	
	@Test
	public void isAdminDelete() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("DELETEADMINSERVICE.No_admin_exists");
		
		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a449";
		String saltString="qw";
		
		User userAdminToBeDeleted=new User();
		userAdminToBeDeleted.setUserId(UUID.fromString(uuid));
		
		AdminEntity adminToBeDeletedEntity=new AdminEntity();
		adminToBeDeletedEntity.setAdminId(null);
		adminToBeDeletedEntity.setAdminIsActive(true);
		
		UsersEntity userAdminEntity=new UsersEntity();
		userAdminEntity.setUserId(UUID.fromString(uuid));
		userAdminEntity.setSaltString(saltString);
		userAdminEntity.setUserAdminId(null);
		
		//***** Using When and then and reurning $NULL$ while called # # # 
		
		Mockito.when(userRepository.findActiveUsers(userAdminToBeDeleted.getUserId())).thenReturn(userAdminEntity);
		
		//***** Calling delteAdmin in AdminSerive $to test the delteAdmin with Invalid UserId$ # # #
		adminService.deleteAdmin(userAdminToBeDeleted,saltString);
		
	}
	
	@Test
	public void isAdminActive() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("DELETEADMINSERVICE.admin_is_not_active");
		
		String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a449";
		String saltString="qw";
		
		User userAdminToBeDeleted=new User();
		userAdminToBeDeleted.setUserId(UUID.fromString(uuid));
		
		AdminEntity adminToBeDeletedEntity=new AdminEntity();
		adminToBeDeletedEntity.setAdminId(BigInteger.valueOf(1));
		adminToBeDeletedEntity.setAdminIsActive(false);
		
		UsersEntity userAdminEntity=new UsersEntity();
		userAdminEntity.setUserId(UUID.fromString(uuid));
		userAdminEntity.setSaltString("qw");
	
		userAdminEntity.setUserAdminId(adminToBeDeletedEntity);
		
		//***** Using When and then and reurning $NULL$ while called # # # 
		
		Mockito.when(userRepository.findActiveUsers(userAdminToBeDeleted.getUserId())).thenReturn(userAdminEntity);
		
		//***** Calling delteAdmin in AdminSerive $to test the delteAdmin with Invalid UserId$ # # #
		adminService.deleteAdmin(userAdminToBeDeleted,saltString);
		
	}
	
	
//	@Test
//	public void isAdminDeletionSuccess() throws Exception
//	{
//		
//	
//	String uuid = "41025101-fd2d-49b5-90e6-487bdcb0a449";
//	String saltString="qw";
//	
//	User userAdminToBeDeleted=new User();
//	userAdminToBeDeleted.setUserId(UUID.fromString(uuid));
//	
//	AdminEntity adminToBeDeletedEntity=new AdminEntity();
//	adminToBeDeletedEntity.setAdminId(BigInteger.valueOf(1));
//	adminToBeDeletedEntity.setAdminIsActive(true);
//	
//	UsersEntity userAdminEntity=new UsersEntity();
//	userAdminEntity.setUserId(UUID.fromString(uuid));
//	userAdminEntity.setSaltString("qw");
//	userAdminEntity.setUserAdminId(adminToBeDeletedEntity);
//	
//	SellerEntity sellerentity = new SellerEntity();
//	sellerentity.setSellerIsActive(true);
//	List<SellerEntity> returnlistforseller= new ArrayList<>();
//	
//	//***** Using When and then and reurning $NULL$ while called # # # 
//	
//	Mockito.when(userRepository.findActiveUsers(userAdminToBeDeleted.getUserId())).thenReturn(userAdminEntity);
//	Mockito.when(adminRepository.getSellerByAdmin(adminToBeDeletedEntity.getAdminId())).thenReturn(returnlistforseller);
//	
//	//***** Calling delteAdmin in AdminSerive $to test the delteAdmin with Invalid UserId$ # # #
//	adminService.deleteAdmin(userAdminToBeDeleted,saltString);
//	
//	
//		
//	}

}
