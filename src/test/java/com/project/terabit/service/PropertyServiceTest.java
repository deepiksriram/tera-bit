package com.project.terabit.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.entity.PropertyEntity;
import com.project.terabit.entity.SellerEntity;
import com.project.terabit.entity.UsersEntity;
import com.project.terabit.model.AddPropertyUser;
import com.project.terabit.model.Image;
import com.project.terabit.model.User;
import com.project.terabit.repository.SellerRepository;
import com.project.terabit.repository.UserRepository;

public class PropertyServiceTest extends TerabitApplicationTests {
	@Mock 
	UserRepository userRepository;
	
	@Mock 
	SellerRepository sellerRepository;
	
	@InjectMocks
	PropertyServiceImpl propertyServiceImpl;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	String saltString="aaaaaaaa";
	
	public AddPropertyUser getAddPropertyUser() {
		
		AddPropertyUser addPropertyUser =new AddPropertyUser();
		addPropertyUser.setPropertyCent(BigInteger.valueOf(1l));
		addPropertyUser.setPropertyCity("MIT");
		addPropertyUser.setPropertyCountry("India");
		addPropertyUser.setPropertyDescription("good neart to urappakkam");
		
		addPropertyUser.setPropertyEsteematedAmount("25");
		List<Image> imageList =new ArrayList<>();
		addPropertyUser.setPropertyImageIds(imageList);
		addPropertyUser.setPropertyLandmark("VGP HOUse");
		addPropertyUser.setPropertyLatitude("10");
		addPropertyUser.setPropertyLongitude("01");
		addPropertyUser.setPropertyOwnedBy(1);
		addPropertyUser.setPropertyState("State");
		addPropertyUser.setPropertyType("type");
		addPropertyUser.setReturnPropertyId(BigInteger.valueOf(1));
		addPropertyUser.setUserid(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445"));
		
		return addPropertyUser;
	}
	
	public User getUser() {
		User user=new User();
		user.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445"));
		user.setSaltstring(saltString);
		return user;
	}
	
	public List<PropertyEntity> getProperty()
	{
		List<PropertyEntity> propertyEntityList=new ArrayList<>();
		PropertyEntity addPropertyUserEntity=new PropertyEntity();
		
		addPropertyUserEntity.setPropertyCent(BigInteger.valueOf(1l));
		addPropertyUserEntity.setPropertyCity("MIT");
		addPropertyUserEntity.setPropertyCountry("India");
		addPropertyUserEntity.setPropertyDescription("good neart to urappakkam");
		addPropertyUserEntity.setPropertyEsteematedAmount("25");
		
		addPropertyUserEntity.setPropertyLandmark("VGP HOUse");
		addPropertyUserEntity.setPropertyLatitude("10");
		addPropertyUserEntity.setPropertyLongitude("01");
		addPropertyUserEntity.setPropertyOwnedBy(1);
		addPropertyUserEntity.setPropertyState("State");
		addPropertyUserEntity.setPropertyType("type");
		
		propertyEntityList.add(addPropertyUserEntity);
		
		return propertyEntityList;
		
		
	}
	public UsersEntity getUserEntity() {
		UsersEntity userEntity=new UsersEntity();
		userEntity.setUserId(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445"));
		userEntity.setSaltString(saltString);
		
		SellerEntity sellerEntity=new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1l));
		sellerEntity.setSellerPropertyId(this.getProperty());
		
		userEntity.setUserSellerId(sellerEntity);
		
		
		return userEntity;
	}
	
	@Test
	public void isSaltStringNull() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.saltstringmissing");
		
		saltString=null;
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
		
	}
	
	@Test
	public void isUserIdNull() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.nouserid");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setUserid(null);
		
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
	}

	@Test
	public void isUserNotExist() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.nouser");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(null);
		
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
		
		
	}
	
	@Test
	public void isSaltStringMissmatch() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.saltstringmissmatch");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		UsersEntity userEntity=this.getUserEntity();
		userEntity.setSaltString(saltString+"a");
		
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
		
		
		
	}
	
	@Test
	public void isUserIsNotSeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SERVICE.user_not_seller");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		SellerEntity sellerEntity = new SellerEntity();
		sellerEntity.setSellerId(BigInteger.valueOf(1l));
		UsersEntity userEntity=this.getUserEntity();
		userEntity.setUserIsSeller(false);
		userEntity.setUserSellerId(sellerEntity);
		
	
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		Mockito.when(sellerRepository.findSellerBySellerId(userEntity.getUserSellerId().getSellerId())).thenReturn(null);
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
	}
	
	
	
	//updatePropertyBySeller
	@Test
	public void isSaltStringNullupdatePropertyBySeller() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.saltstringmissing");
		
		saltString=null;
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
		
	}
	
	@Test
	public void isUserIdNullupdatePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.nouserid");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setUserid(null);
		
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
	}

	@Test
	public void isUserNotExistupdatePropertyBySeller() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.nouser");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(null);
		
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
		
		
	}
	
	@Test
	public void isSaltStringMissmatchupdatePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.saltstringmissmatch");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		UsersEntity userEntity=this.getUserEntity();
		userEntity.setSaltString(saltString+"a");
		
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
		
		
		
	}
	
	@Test
	public void isUserIsNotSellerupdatePropertyBySeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SERVICE.user_not_seller");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		UsersEntity userEntity=this.getUserEntity();
		userEntity.setUserIsSeller(false);
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
	}
	
	//getPropertyBySeller
	@Test
	public void isSaltStringNullgetPropertyBySeller() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.saltstringmissing");
		
		saltString=null;
		
		User user=this.getUser();
		
		
		propertyServiceImpl.getPropertyBySeller(user, saltString);
		
	}
	
	@Test
	public void isUserIdNullgetPropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.nouserid");
		
		User user=this.getUser();
		user.setUserId(null);
		
		propertyServiceImpl.getPropertyBySeller(user, saltString);
	}

//	@Test
//	public void isUserNotExistGetPropertyBySeller() throws Exception{
//		
//		expectedException.expect(Exception.class);
//		expectedException.expectMessage("ADDPROPERTYSERVICE.nouser");
//		
//		User user=this.getUser();
//		
//		Mockito.when(userRepository.findUserByUserId(user.getUserId())).thenReturn(null);
//		
//		propertyServiceImpl.getPropertyBySeller(user, saltString);
//		
//		
//	}
	
	@Test
	public void isSaltStringMissmatchGetPropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.saltstringmissmatch");
		
		User user=this.getUser();
		
		UsersEntity userEntity=this.getUserEntity();
		userEntity.setSaltString(saltString+"a");
		
		
		Mockito.when(userRepository.findUserByUserId(user.getUserId())).thenReturn(userEntity);
		
		propertyServiceImpl.getPropertyBySeller(user, saltString);
		
		
		
	}
	
	@Test
	public void isUserIsNotSellerGetPropertyBySeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SERVICE.user_not_seller");
		
		User user=this.getUser();
		
		UsersEntity userEntity=this.getUserEntity();
		
		userEntity.setUserIsSeller(false);
		
		Mockito.when(userRepository.findUserByUserId(user.getUserId())).thenReturn(userEntity);
		propertyServiceImpl.getPropertyBySeller(user, saltString);
	}
	
	//delete
	
	@Test
	public void isSaltStringNullDeletePropertyBySeller() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.saltstringmissing");
		
		saltString=null;
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		propertyServiceImpl.deletePropertyBySeller(addPropertyUser, saltString);
		
	}
	
	@Test
	public void isUserIdNullDeletePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.nouserid");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setUserid(null);
		
		propertyServiceImpl.deletePropertyBySeller(addPropertyUser, saltString);
	}

	@Test
	public void isUserNotExistDeletePropertyBySeller() throws Exception{
		
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.nouser");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(null);
		
		propertyServiceImpl.deletePropertyBySeller(addPropertyUser, saltString);
		
		
	}
	
	@Test
	public void isSaltStringMissmatchDeletePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("PROPERTY.saltstringmissmatch");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		
		UsersEntity userEntity=this.getUserEntity();
		userEntity.setSaltString(saltString+"a");
		
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		
		propertyServiceImpl.deletePropertyBySeller(addPropertyUser, saltString);
		
		
		
	}
	
	@Test
	public void isUserIsNotSellerDeletePropertyBySeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SERVICE.user_not_seller");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setUserid(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445"));
		UsersEntity userEntity=this.getUserEntity();
		userEntity.setUserIsSeller(false);
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.deletePropertyBySeller(addPropertyUser, saltString);
	}
	
	
	
	//AddValidate
//	@Test
//	public void isValidUserIdAdd() throws Exception
//	{
//		expectedException.expect(Exception.class);
//		expectedException.expectMessage("VALIDATOR.invalid_user_id");
//		
//		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
//		addPropertyUser.setUserid(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445"));
//		
//		UsersEntity userEntity=this.getUserEntity();
//		
//		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
//		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
//		
//		
//			
//	}
	
	@Test
	public void isValidPropertyOwnedByAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_owned_by");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyOwnedBy(3);
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
	}
	

	@Test
	public void isValidCityAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_city");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyCity("1234");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
	}
	
	@Test
	public void isValidStateAdd() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_state_or_country");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyState("1234");
		
		System.out.println("Test1");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
	}
	
	@Test
	public void isValidCountryAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_state_or_country");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyCountry("1234");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
		
	}
	
	@Test
	public void isValidPropertyLatitudeAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_latitude_or_longitude");
		
		AddPropertyUser addPropertyUser=this.getAddPropertyUser();
		addPropertyUser.setPropertyLatitude("solai");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
	}
	
	@Test
	public void isValidPropertyLongitudeAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_latitude_or_longitude");
		
		AddPropertyUser addPropertyUser=this.getAddPropertyUser();
		addPropertyUser.setPropertyLongitude("solai");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
	}
	
	@Test
	public void isValidPropertyEstimatedAmountAdd() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_esteemated_amount");
		
		AddPropertyUser addPropertyUser=this.getAddPropertyUser();
		addPropertyUser.setPropertyEsteematedAmount("solai");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
	}
	
//Update
//	@Test
//	public void isValidUserIdUpdatePropertyBySeller() throws Exception
//	{
//		expectedException.expect(Exception.class);
//		expectedException.expectMessage("VALIDATOR.invalid_user_id");
//		
//		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
//		addPropertyUser.setUserid(UUID.fromString("41025101-fd2d-49b5-90e6-487bdcb0a445"));
//		
//		UsersEntity userEntity=this.getUserEntity();
//		
//		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
//		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
//		
//	}
	
	@Test
	public void isValidPropertyOwnedByUpdatePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_owned_by");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyOwnedBy(3);
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
	}
	

	@Test
	public void isValidCityUpdatePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_city");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyCity("1234");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.addPropertyBySeller(addPropertyUser, saltString);
	}
	
	@Test
	public void isValidStateUpdatePropertyBySeller() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_state_or_country");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyState("1234");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
	}
	
	@Test
	public void isValidCountryUpdatePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_state_or_country");
		
		AddPropertyUser addPropertyUser =this.getAddPropertyUser();
		addPropertyUser.setPropertyCountry("1234");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
		
	}
	
	@Test
	public void isValidPropertyLatitudeUpdatePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_latitude_or_longitude");
		
		AddPropertyUser addPropertyUser=this.getAddPropertyUser();
		addPropertyUser.setPropertyLatitude("solai");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
	}
	
	@Test
	public void isValidPropertyLongitudeUpdatePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_latitude_or_longitude");
		
		AddPropertyUser addPropertyUser=this.getAddPropertyUser();
		addPropertyUser.setPropertyLongitude("solai");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
	}
	
	@Test
	public void isValidPropertyEstimatedAmountUpdatePropertyBySeller() throws Exception
	{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("VALIDATOR.invalid_esteemated_amount");
		
		AddPropertyUser addPropertyUser=this.getAddPropertyUser();
		addPropertyUser.setPropertyEsteematedAmount("solai");
		
		UsersEntity userEntity=this.getUserEntity();
		
		Mockito.when(userRepository.findUserByUserId(addPropertyUser.getUserid())).thenReturn(userEntity);
		propertyServiceImpl.updatePropertyBySeller(addPropertyUser, saltString);
	}




}
