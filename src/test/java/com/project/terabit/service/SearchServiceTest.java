package com.project.terabit.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.model.Search;
import com.project.terabit.repository.PropertyRepository;
import com.project.terabit.repository.SearchRepository;
import com.project.terabit.repository.UserRepository;

public class SearchServiceTest extends TerabitApplicationTests{

	
	@Mock 
	UserRepository userRepository;
	
	@Mock
	PropertyRepository propertyrepository;
	
	@Mock
	SearchRepository searchRepository;
	
	@InjectMocks
	SearchServiceImpl service;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void isKeyWordNull() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SEARCHSERVICE.invalid_keyword");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		Search search = new Search();
		search.setKeyword(null);
		search.setUserId(UUID.fromString(uuid));
		String saltstring = null;
		
		service.search(search, saltstring);
	}
	
	@Test
	public void isSaltstringNull() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SEARCHSERVICE.saltstring_not_provided");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		String keyword = "key";
		Search search = new Search();
		search.setKeyword(keyword);
		search.setUserId(UUID.fromString(uuid));
		search.setPropertyId(BigInteger.valueOf(0l));
		String saltstring = null;
		List<BigInteger> propertyList = new ArrayList<>();

		
		Mockito.when(searchRepository.getPropertyIdBySearch(search.getKeyword(),search.getPropertyId())).thenReturn(propertyList);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.search(search, saltstring);
	}
	
	@Test
	public void isSaltstringMissmatch() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SEARCHSERVICE.invalid_user");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		String keyword = "key";
		Search search = new Search();
		search.setKeyword(keyword);
		search.setPropertyId(BigInteger.valueOf(0l));
		search.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		List<BigInteger> propertyList = new ArrayList<>();
		
		Mockito.when(searchRepository.getPropertyIdBySearch(search.getKeyword(),search.getPropertyId())).thenReturn(propertyList);
		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.search(search, saltstring);
	}
	
	@Test
	public void isUserIdNotPresent() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SEARCHSERVICE.invalid_user");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		String keyword = "key";
		Search search = new Search();
		search.setKeyword(keyword);
		search.setUserId(UUID.fromString(uuid));
		search.setPropertyId(BigInteger.valueOf(0l));
		String saltstring = "aaa";
		List<BigInteger> propertyList = new ArrayList<>();

		Mockito.when(searchRepository.getPropertyIdBySearch(search.getKeyword(),search.getPropertyId())).thenReturn(propertyList);

		Mockito.when(userRepository.findUserById(UUID.fromString(uuid),saltstring)).thenReturn(null);
		service.search(search, saltstring);
	}
	
	@Test
	public void KeywordNull() throws Exception{
		expectedException.expect(Exception.class);
		expectedException.expectMessage("SEARCHSERVICE.invalid_keyword");
		String uuid = "88b72c22-cc4c-46b1-94ae-c38244eb6fbb";
		String keyword = null;
		Search search = new Search();
		search.setKeyword(keyword);
		search.setUserId(UUID.fromString(uuid));
		String saltstring = "aaa";
		
		service.search(search, saltstring);
	}
}
