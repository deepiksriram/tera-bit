package com.project.terabit.validatorTest;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.model.User;
import com.project.terabit.repository.UserRepository;

import com.project.terabit.service.SignUpUserServiceImpl;
import com.project.terabit.validator.UpdateUserValidator;

public class UpdateUserValidatorTest extends TerabitApplicationTests {
	@Mock
	UserRepository userrepository;
	
	@InjectMocks
	SignUpUserServiceImpl signupservice;
	
	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	String salt_string="aaaaaaaa";
	UUID uuid = UUID.fromString("88b72c22-cc4c-46b1-94ae-c38244eb6fbb");
	
	 public User getUser() {
		 User user = new User();
		 
	     user.setUserEmailId("lisadeepik@gmail.com");
	     user.setUserContactNo(BigInteger.valueOf(8681999437l));
	     user.setUserPassword("Deepik@lisa79");
	     user.setSaltstring(salt_string);
	     user.setUserId(uuid);
	     user.setUserFirstName("solai");
	     user.setUserLastName("ganesh");
	     
	     return user;
	 }
	 
	 @Test
	 public void isValidEmailIdNormalStringUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai");
	     
	     UpdateUserValidator.updateInfoValidate(user_to_be_saved_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutSCUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai.com");
	     
	     UpdateUserValidator.updateInfoValidate(user_to_be_saved_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithoutDotUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai@gmailcom");
	     
	     UpdateUserValidator.updateInfoValidate(user_to_be_saved_in);
	     
	 }
	 @Test
	 public void isValidEmailIdWithExtraLengthAfterDotUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_email_id");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserEmailId("solai@gmail.comin");
	     
	     UpdateUserValidator.updateInfoValidate(user_to_be_saved_in);
	     
	 }
	 
	 @Test
	 public void isValidContactNumberLessLengthUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_phone_no");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserContactNo(BigInteger.valueOf(868199943l));
	     

	     UpdateUserValidator.updateInfoValidate(user_to_be_saved_in);
	     
		 
	 }
	 
	 @Test
	 public void isValidContactNumberMoreLengthUpdate() throws Exception{
		 expectedException.expect(Exception.class);
	     expectedException.expectMessage("UPDATEUSERVALIDATOR.invalid_phone_no");
	     
	     User user_to_be_saved_in=this.getUser();
	     user_to_be_saved_in.setUserContactNo(BigInteger.valueOf(868199943333l));
	     
	     UpdateUserValidator.updateInfoValidate(user_to_be_saved_in);
	     
		 
	 }
	


}
