package com.project.terabit.validatorTest;

import java.math.BigInteger;
import java.util.UUID;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


import com.project.terabit.TerabitApplicationTests;
import com.project.terabit.model.User;

import com.project.terabit.validator.SignUpUserValidator;

public class SignUpUserValidatorTest extends TerabitApplicationTests{
       
       
     @Rule
     public ExpectedException expectedException = ExpectedException.none();
     
     String salt_string="aaaaaaaa";
 	 UUID uuid = UUID.fromString("88b72c22-cc4c-46b1-94ae-c38244eb6fbb");
 	
 	 public User getUser() {
 		 User user = new User();
 		 
 	     user.setUserEmailId("lisadeepik@gmail.com");
 	     user.setUserContactNo(BigInteger.valueOf(8681999437l));
 	     user.setUserPassword("Deepik@lisa79");
 	     user.setSaltstring(salt_string);
 	     user.setUserId(uuid);
 	     user.setUserFirstName("solai");
 	     user.setUserLastName("ganesh");
 	     
 	     return user;
 	 }
       
  	 @Test
  	 public void isValidFirstNameInterger() throws Exception {
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_firstname");
  	     
  	     User user =this.getUser();
  	     user.setUserFirstName("sol5gb");
  	     
  	     SignUpUserValidator.validate(user);
  	     
  	}
  	 @Test
  	 public void isValidFirstNameSpecialChar() throws Exception {
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_firstname");
  	     
  	     User user =this.getUser();
  	     user.setUserFirstName("sol*gb");
  	     
  	     SignUpUserValidator.validate(user);
  	     
  		 
  	 }
  	 @Test
  	 public void isValidLastNameSpecialChar() throws Exception {
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_lastname");
  	     
  	     User user =this.getUser();
  	     user.setUserLastName("sol*gb");
  	     
  	     SignUpUserValidator.validate(user);
  	     
  		 
  	 }
  	 @Test
  	 public void isValidLastNameInterger() throws Exception {
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_lastname");
  	     
  	     User user =this.getUser();
  	     user.setUserLastName("sol5gb");
  	     
  	     SignUpUserValidator.validate(user);
  	     
  		 
  	 }
  	 
  	 @Test
  	 public void isValidEmailIdNormalString() throws Exception{
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_email_id");
  	     
  	     User user_to_be_saved_in=this.getUser();
  	     user_to_be_saved_in.setUserEmailId("solai");
  	     
  	     SignUpUserValidator.validate(user_to_be_saved_in);
  	     
  	 }
  	 @Test
  	 public void isValidEmailIdWithoutSC() throws Exception{
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_email_id");
  	     
  	     User user_to_be_saved_in=this.getUser();
  	     user_to_be_saved_in.setUserEmailId("solai.com");
  	     
  	     SignUpUserValidator.validate(user_to_be_saved_in);
  	     
  	 }
  	 @Test
  	 public void isValidEmailIdWithoutDot() throws Exception{
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_email_id");
  	     
  	     User user_to_be_saved_in=this.getUser();
  	     user_to_be_saved_in.setUserEmailId("solai@gmailcom");
  	     
  	     SignUpUserValidator.validate(user_to_be_saved_in);
  	     
  	 }
  	 @Test
  	 public void isValidEmailIdWithExtraLengthAfterDot() throws Exception{
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_email_id");
  	     
  	     User user_to_be_saved_in=this.getUser();
  	     user_to_be_saved_in.setUserEmailId("solai@gmail.comin");
  	     
  	     SignUpUserValidator.validate(user_to_be_saved_in);
  	     
  	 }
  	 
  	 @Test
  	 public void isValidContactNumberLessLength() throws Exception{
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_phone_no");
  	     
  	     User user_to_be_saved_in=this.getUser();
  	     user_to_be_saved_in.setUserContactNo(BigInteger.valueOf(868199943l));
  	     
  	     SignUpUserValidator.validate(user_to_be_saved_in);
  	     
  		 
  	 }
  	 
  	 @Test
  	 public void isValidContactNumberMoreLength() throws Exception{
  		 expectedException.expect(Exception.class);
  	     expectedException.expectMessage("SIGNUPVALIDATOR.invalid_phone_no");
  	     
  	     User user_to_be_saved_in=this.getUser();
  	     user_to_be_saved_in.setUserContactNo(BigInteger.valueOf(868199943333l));
  	     
  	     SignUpUserValidator.validate(user_to_be_saved_in);
  	     
  		 
  	 }

}
